﻿namespace BrownianModel2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cBControlVar = new System.Windows.Forms.CheckBox();
            this.cBAnti = new System.Windows.Forms.CheckBox();
            this.rTBresults = new System.Windows.Forms.RichTextBox();
            this.rdbPut = new System.Windows.Forms.RadioButton();
            this.raBCall = new System.Windows.Forms.RadioButton();
            this.BtnCalculate = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TbPaths = new System.Windows.Forms.TextBox();
            this.TbSteps = new System.Windows.Forms.TextBox();
            this.TbRate = new System.Windows.Forms.TextBox();
            this.TbTenor = new System.Windows.Forms.TextBox();
            this.TbVol = new System.Windows.Forms.TextBox();
            this.TbStrike = new System.Windows.Forms.TextBox();
            this.tbUnder = new System.Windows.Forms.TextBox();
            this.cBMulti = new System.Windows.Forms.CheckBox();
            this.pB1 = new System.Windows.Forms.ProgressBar();
            this.euro = new System.Windows.Forms.RadioButton();
            this.rbasian = new System.Windows.Forms.RadioButton();
            this.Barrier = new System.Windows.Forms.RadioButton();
            this.digital = new System.Windows.Forms.RadioButton();
            this.lookback = new System.Windows.Forms.RadioButton();
            this.range = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TbBarrier = new System.Windows.Forms.TextBox();
            this.downando = new System.Windows.Forms.RadioButton();
            this.Upando = new System.Windows.Forms.RadioButton();
            this.Dandi = new System.Windows.Forms.RadioButton();
            this.Upandi = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.TBDigital = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // cBControlVar
            // 
            this.cBControlVar.AutoSize = true;
            this.cBControlVar.Location = new System.Drawing.Point(439, 407);
            this.cBControlVar.Name = "cBControlVar";
            this.cBControlVar.Size = new System.Drawing.Size(141, 24);
            this.cBControlVar.TabIndex = 64;
            this.cBControlVar.Text = "Control Variate";
            this.cBControlVar.UseVisualStyleBackColor = true;
            // 
            // cBAnti
            // 
            this.cBAnti.AutoSize = true;
            this.cBAnti.Location = new System.Drawing.Point(240, 407);
            this.cBAnti.Name = "cBAnti";
            this.cBAnti.Size = new System.Drawing.Size(102, 24);
            this.cBAnti.TabIndex = 63;
            this.cBAnti.Text = "Antithetic";
            this.cBAnti.UseVisualStyleBackColor = true;
            // 
            // rTBresults
            // 
            this.rTBresults.Location = new System.Drawing.Point(127, 649);
            this.rTBresults.Name = "rTBresults";
            this.rTBresults.Size = new System.Drawing.Size(768, 159);
            this.rTBresults.TabIndex = 62;
            this.rTBresults.Text = "";
            // 
            // rdbPut
            // 
            this.rdbPut.AutoSize = true;
            this.rdbPut.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rdbPut.Location = new System.Drawing.Point(569, 457);
            this.rdbPut.Name = "rdbPut";
            this.rdbPut.Size = new System.Drawing.Size(109, 24);
            this.rdbPut.TabIndex = 61;
            this.rdbPut.Text = "Put Option";
            this.rdbPut.UseVisualStyleBackColor = true;
            // 
            // raBCall
            // 
            this.raBCall.AutoSize = true;
            this.raBCall.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.raBCall.Location = new System.Drawing.Point(335, 457);
            this.raBCall.Name = "raBCall";
            this.raBCall.Size = new System.Drawing.Size(111, 24);
            this.raBCall.TabIndex = 60;
            this.raBCall.Text = "Call Option";
            this.raBCall.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.raBCall.UseVisualStyleBackColor = true;
            // 
            // BtnCalculate
            // 
            this.BtnCalculate.Location = new System.Drawing.Point(397, 519);
            this.BtnCalculate.Name = "BtnCalculate";
            this.BtnCalculate.Size = new System.Drawing.Size(259, 100);
            this.BtnCalculate.TabIndex = 59;
            this.BtnCalculate.Text = "Calculate";
            this.BtnCalculate.UseVisualStyleBackColor = true;
            this.BtnCalculate.Click += new System.EventHandler(this.BtnCalculate_Click_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(146, 223);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(132, 20);
            this.label7.TabIndex = 58;
            this.label7.Text = "Number of Paths:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(513, 172);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(133, 20);
            this.label6.TabIndex = 57;
            this.label6.Text = "Number of Steps:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(156, 167);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 20);
            this.label5.TabIndex = 56;
            this.label5.Text = "Tenor (in years):";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(446, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(210, 20);
            this.label4.TabIndex = 55;
            this.label4.Text = "Risk-Free Rate (as percent):";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(123, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 20);
            this.label3.TabIndex = 54;
            this.label3.Text = "Volatility (as percent):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(553, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 20);
            this.label2.TabIndex = 53;
            this.label2.Text = "Strike Price:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(156, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 20);
            this.label1.TabIndex = 52;
            this.label1.Text = "Underlying Price:";
            // 
            // TbPaths
            // 
            this.TbPaths.Location = new System.Drawing.Point(289, 220);
            this.TbPaths.Name = "TbPaths";
            this.TbPaths.Size = new System.Drawing.Size(131, 26);
            this.TbPaths.TabIndex = 51;
            // 
            // TbSteps
            // 
            this.TbSteps.Location = new System.Drawing.Point(662, 166);
            this.TbSteps.Name = "TbSteps";
            this.TbSteps.Size = new System.Drawing.Size(131, 26);
            this.TbSteps.TabIndex = 50;
            // 
            // TbRate
            // 
            this.TbRate.Location = new System.Drawing.Point(662, 110);
            this.TbRate.Name = "TbRate";
            this.TbRate.Size = new System.Drawing.Size(131, 26);
            this.TbRate.TabIndex = 49;
            // 
            // TbTenor
            // 
            this.TbTenor.Location = new System.Drawing.Point(289, 164);
            this.TbTenor.Name = "TbTenor";
            this.TbTenor.Size = new System.Drawing.Size(131, 26);
            this.TbTenor.TabIndex = 48;
            // 
            // TbVol
            // 
            this.TbVol.Location = new System.Drawing.Point(289, 107);
            this.TbVol.Name = "TbVol";
            this.TbVol.Size = new System.Drawing.Size(131, 26);
            this.TbVol.TabIndex = 47;
            // 
            // TbStrike
            // 
            this.TbStrike.Location = new System.Drawing.Point(662, 56);
            this.TbStrike.Name = "TbStrike";
            this.TbStrike.Size = new System.Drawing.Size(131, 26);
            this.TbStrike.TabIndex = 46;
            // 
            // tbUnder
            // 
            this.tbUnder.Location = new System.Drawing.Point(289, 56);
            this.tbUnder.Name = "tbUnder";
            this.tbUnder.Size = new System.Drawing.Size(131, 26);
            this.tbUnder.TabIndex = 45;
            // 
            // cBMulti
            // 
            this.cBMulti.AutoSize = true;
            this.cBMulti.Location = new System.Drawing.Point(649, 407);
            this.cBMulti.Name = "cBMulti";
            this.cBMulti.Size = new System.Drawing.Size(144, 24);
            this.cBMulti.TabIndex = 65;
            this.cBMulti.Text = "Multi-Threading";
            this.cBMulti.UseVisualStyleBackColor = true;
            // 
            // pB1
            // 
            this.pB1.Location = new System.Drawing.Point(289, 840);
            this.pB1.Name = "pB1";
            this.pB1.Size = new System.Drawing.Size(487, 23);
            this.pB1.TabIndex = 66;
            // 
            // euro
            // 
            this.euro.AutoSize = true;
            this.euro.Location = new System.Drawing.Point(27, 28);
            this.euro.Name = "euro";
            this.euro.Size = new System.Drawing.Size(104, 24);
            this.euro.TabIndex = 67;
            this.euro.TabStop = true;
            this.euro.Text = "European";
            this.euro.UseVisualStyleBackColor = true;
            // 
            // rbasian
            // 
            this.rbasian.AutoSize = true;
            this.rbasian.Location = new System.Drawing.Point(175, 28);
            this.rbasian.Name = "rbasian";
            this.rbasian.Size = new System.Drawing.Size(74, 24);
            this.rbasian.TabIndex = 68;
            this.rbasian.TabStop = true;
            this.rbasian.Text = "Asian";
            this.rbasian.UseVisualStyleBackColor = true;
            // 
            // Barrier
            // 
            this.Barrier.AutoSize = true;
            this.Barrier.Location = new System.Drawing.Point(346, 26);
            this.Barrier.Name = "Barrier";
            this.Barrier.Size = new System.Drawing.Size(81, 24);
            this.Barrier.TabIndex = 69;
            this.Barrier.TabStop = true;
            this.Barrier.Text = "Barrier";
            this.Barrier.UseVisualStyleBackColor = true;
            // 
            // digital
            // 
            this.digital.AutoSize = true;
            this.digital.Location = new System.Drawing.Point(27, 77);
            this.digital.Name = "digital";
            this.digital.Size = new System.Drawing.Size(78, 24);
            this.digital.TabIndex = 70;
            this.digital.TabStop = true;
            this.digital.Text = "Digital";
            this.digital.UseVisualStyleBackColor = true;
            // 
            // lookback
            // 
            this.lookback.AutoSize = true;
            this.lookback.Location = new System.Drawing.Point(175, 77);
            this.lookback.Name = "lookback";
            this.lookback.Size = new System.Drawing.Size(103, 24);
            this.lookback.TabIndex = 71;
            this.lookback.TabStop = true;
            this.lookback.Text = "Lookback";
            this.lookback.UseVisualStyleBackColor = true;
            // 
            // range
            // 
            this.range.AutoSize = true;
            this.range.Location = new System.Drawing.Point(346, 77);
            this.range.Name = "range";
            this.range.Size = new System.Drawing.Size(82, 24);
            this.range.TabIndex = 72;
            this.range.TabStop = true;
            this.range.Text = "Range";
            this.range.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Barrier);
            this.groupBox1.Controls.Add(this.range);
            this.groupBox1.Controls.Add(this.rbasian);
            this.groupBox1.Controls.Add(this.lookback);
            this.groupBox1.Controls.Add(this.euro);
            this.groupBox1.Controls.Add(this.digital);
            this.groupBox1.Location = new System.Drawing.Point(274, 262);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(543, 127);
            this.groupBox1.TabIndex = 73;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Option Type";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.TbBarrier);
            this.groupBox2.Controls.Add(this.downando);
            this.groupBox2.Controls.Add(this.Upando);
            this.groupBox2.Controls.Add(this.Dandi);
            this.groupBox2.Controls.Add(this.Upandi);
            this.groupBox2.Location = new System.Drawing.Point(908, 72);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(131, 328);
            this.groupBox2.TabIndex = 74;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Barrier Options";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 238);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 20);
            this.label8.TabIndex = 5;
            this.label8.Text = "Barrier Value";
            // 
            // TbBarrier
            // 
            this.TbBarrier.Location = new System.Drawing.Point(16, 261);
            this.TbBarrier.Name = "TbBarrier";
            this.TbBarrier.Size = new System.Drawing.Size(100, 26);
            this.TbBarrier.TabIndex = 4;
            // 
            // downando
            // 
            this.downando.AutoSize = true;
            this.downando.Location = new System.Drawing.Point(0, 184);
            this.downando.Name = "downando";
            this.downando.Size = new System.Drawing.Size(136, 24);
            this.downando.TabIndex = 3;
            this.downando.TabStop = true;
            this.downando.Text = "Down and Out";
            this.downando.UseVisualStyleBackColor = true;
            // 
            // Upando
            // 
            this.Upando.AutoSize = true;
            this.Upando.Location = new System.Drawing.Point(0, 132);
            this.Upando.Name = "Upando";
            this.Upando.Size = new System.Drawing.Size(116, 24);
            this.Upando.TabIndex = 2;
            this.Upando.TabStop = true;
            this.Upando.Text = "Up and Out";
            this.Upando.UseVisualStyleBackColor = true;
            // 
            // Dandi
            // 
            this.Dandi.AutoSize = true;
            this.Dandi.Location = new System.Drawing.Point(0, 86);
            this.Dandi.Name = "Dandi";
            this.Dandi.Size = new System.Drawing.Size(124, 24);
            this.Dandi.TabIndex = 1;
            this.Dandi.TabStop = true;
            this.Dandi.Text = "Down and In";
            this.Dandi.UseVisualStyleBackColor = true;
            // 
            // Upandi
            // 
            this.Upandi.AutoSize = true;
            this.Upandi.Location = new System.Drawing.Point(0, 39);
            this.Upandi.Name = "Upandi";
            this.Upandi.Size = new System.Drawing.Size(104, 24);
            this.Upandi.TabIndex = 0;
            this.Upandi.TabStop = true;
            this.Upandi.Text = "Up and In";
            this.Upandi.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.TBDigital);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Location = new System.Drawing.Point(908, 430);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(131, 85);
            this.groupBox3.TabIndex = 75;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Digital Rebate";
            // 
            // TBDigital
            // 
            this.TBDigital.Location = new System.Drawing.Point(13, 37);
            this.TBDigital.Name = "TBDigital";
            this.TBDigital.Size = new System.Drawing.Size(100, 26);
            this.TBDigital.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 43);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 20);
            this.label9.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1112, 910);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pB1);
            this.Controls.Add(this.cBMulti);
            this.Controls.Add(this.cBControlVar);
            this.Controls.Add(this.cBAnti);
            this.Controls.Add(this.rTBresults);
            this.Controls.Add(this.rdbPut);
            this.Controls.Add(this.raBCall);
            this.Controls.Add(this.BtnCalculate);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TbPaths);
            this.Controls.Add(this.TbSteps);
            this.Controls.Add(this.TbRate);
            this.Controls.Add(this.TbTenor);
            this.Controls.Add(this.TbVol);
            this.Controls.Add(this.TbStrike);
            this.Controls.Add(this.tbUnder);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TbPaths;
        private System.Windows.Forms.TextBox TbSteps;
        private System.Windows.Forms.TextBox TbRate;
        private System.Windows.Forms.TextBox TbTenor;
        private System.Windows.Forms.TextBox TbVol;
        private System.Windows.Forms.TextBox tbUnder;
        public System.Windows.Forms.TextBox TbStrike;
        public System.Windows.Forms.RichTextBox rTBresults;
        public System.Windows.Forms.ProgressBar pB1;
        public System.Windows.Forms.CheckBox cBControlVar;
        public System.Windows.Forms.CheckBox cBAnti;
        public System.Windows.Forms.RadioButton rdbPut;
        public System.Windows.Forms.RadioButton raBCall;
        public System.Windows.Forms.Button BtnCalculate;
        public System.Windows.Forms.CheckBox cBMulti;
        private System.Windows.Forms.RadioButton euro;
        private System.Windows.Forms.RadioButton rbasian;
        private System.Windows.Forms.RadioButton Barrier;
        private System.Windows.Forms.RadioButton digital;
        private System.Windows.Forms.RadioButton lookback;
        private System.Windows.Forms.RadioButton range;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TbBarrier;
        private System.Windows.Forms.RadioButton downando;
        private System.Windows.Forms.RadioButton Upando;
        private System.Windows.Forms.RadioButton Dandi;
        private System.Windows.Forms.RadioButton Upandi;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox TBDigital;
        private System.Windows.Forms.Label label9;
    }
}

