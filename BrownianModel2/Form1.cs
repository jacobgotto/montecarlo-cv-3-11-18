﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;

namespace BrownianModel2
{
    public partial class Form1 : Form
    {
        public double S { get; set; }
        public double K { get; set; }
        public double r { get; set; }
        public double v { get; set; }
        public double T { get; set; }
        public int steps { get; set; }
        public int paths { get; set; }
        public int y { get; set; }
        public int cv { get; set; }
        public int type { get; set; }
        public int bartype { get; set; }
        public double rebate { get; set; }
        public double bar { get; set; }


        MultiCore mult = new MultiCore();
        MultiCore1 mult1 = new MultiCore1();
        MultiCore2 mult2 = new MultiCore2();

        public Form1()
        {
            InitializeComponent();
        }


        private void BtnCalculate_Click_1(object sender, EventArgs e)
        {
            pB1.Increment(-100);
            mult.cores = System.Environment.ProcessorCount;
            Stopwatch time = new Stopwatch();
            time.Start();
            try
            {
                S = Convert.ToDouble(tbUnder.Text);
            }

            catch
            {
                rTBresults.Text = "Error, please enter Underlying Price as a number";
                return;
            }
            try
            {
                K = Convert.ToDouble(TbStrike.Text);

            }
            catch
            {
                rTBresults.Text = "Error, please enter Strike Price as a number";
                return;
            }
            try
            {
                v = Convert.ToDouble(TbVol.Text);

            }
            catch
            {
                rTBresults.Text = "Error, please enter Volatility as a decimal";
                return;
            }
            try
            {
                r = Convert.ToDouble(TbRate.Text);

            }
            catch
            {
                rTBresults.Text = "Error, please enter Risk-Free Rate as a decimal";
                return;
            }
            try
            {
                T = Convert.ToDouble(TbTenor.Text);

            }
            catch
            {
                rTBresults.Text = "Error, please enter Tenor as a number";
                return;
            }
            try
            {
                steps = Convert.ToInt32(TbSteps.Text);

            }
            catch
            {
                rTBresults.Text = "Error, please enter Number of Steps as an integer";
                return;
            }
            try
            {
                paths = Convert.ToInt32(TbPaths.Text);

            }
            catch
            {
                rTBresults.Text = "Error, please enter Number of Paths as an integer";
                return;
            }
            if (Barrier.Checked == true)
            {
                try
                {

                    bar = Convert.ToDouble(TbBarrier.Text);
                


                }
                catch
                {
                    rTBresults.Text = "Error, please enter a barrier";
                    return;
                }
            }
            try
            {

                if (Barrier.Checked == true)
                {
                    try
                    {
                        if (Upandi.Checked == true)
                        {
                            bartype = 0;
                            mult2.bartype = 0;
                        }
                        else if (Upando.Checked == true)
                        {
                            bartype = 1;
                            mult2.bartype = 1;
                        }
                        else if (Dandi.Checked == true)
                        {
                            bartype = 2;
                            mult2.bartype = 2;
                        }
                        else if (downando.Checked == true)
                        {
                            bartype = 3;
                            mult2.bartype = 3;
                        }
                        else
                        {
                            double[] thr = new double[2];
                            for (int i = 0; i < 4; i++)
                            {
                                thr[i] = 6;
                            }
                        }
                    }


                    catch
                    {

                        rTBresults.Text = "PLease select a Barrier Type";
                        return;
                    }
                }
            }

            catch 
            {

                throw;
            }

        
            if (digital.Checked == true)
            {
                try
                {
                    rebate = Convert.ToDouble(TBDigital.Text);
                    mult1.rebate = Convert.ToDouble(TBDigital.Text);
                }
                catch
                {
                    rTBresults.Text = "Error, please enter a rebate";
                    return;

                }
            }



            if (cBControlVar.Checked == true)
            {
                cv = 0;
            }
            else
            {
                cv = 1;
            }
            if (cBAnti.Checked == true)
            {
                y = 0;
            }
            else
            {
                y = 1;
            }
            if (raBCall.Checked)
            {
                type = 0;
            }
            else
            {
                type = 1;
            }
            mult.s = S;
            mult.k = K;
            mult.v = v;
            mult.r = r;
            mult.T = T;
            mult.steps = steps;
            mult.paths = paths;
            mult1.s = S;
            mult1.k = K;
            mult1.v = v;
            mult1.r = r;
            mult1.T = T;
            mult1.steps = steps;
            mult1.paths = paths;

            mult2.s = S;
            mult2.k = K;
            mult2.v = v;
            mult2.r = r;
            mult2.T = T;
            mult2.steps = steps;
            mult2.paths = paths;
            mult2.bartype = bartype;
            mult2.bar = bar;

            if (euro.Checked == true)
            {
                if (raBCall.Checked == true)
                {
                    int type = 0;
                    mult.type = 0;
                    double[,] w = new double[paths, steps];
                    double[] price, price1, price2, d1, d2, v1, v2, r1, r2, t1 = new double[2];
                    Random1 rnd = new Random1();
                    EuroOption euro2 = new EuroOption();
                    if (cBAnti.Checked == true && cBControlVar.Checked == false)
                    {
                        w = rnd.PolarRejectAnti(paths, steps);
                    }
                    else
                    {
                        w = rnd.PolarReject(paths, steps);
                    }
                    if (cBMulti.Checked == true && cBAnti.Checked == true && cBControlVar.Checked == true)
                    {
                        price = mult.Pricing(mult.payoff1, type, cv, y);
                        price2 = euro2.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(11);
                        price1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S + 0.001 * S;
                        d1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S - 0.001 * S;
                        d2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(12);
                        mult.s = S;
                        mult.v = v + v * 0.001;
                        v1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.v = v - v * 0.001;
                        v2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.v = v;
                        mult.T = T + T * 0.001;
                        t1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.T = T;
                        mult.r = r + r * 0.001;
                        r1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.r = r - r * 0.001;
                        r2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.r = r;
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price1[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                         "Theta: " + Convert.ToString((t1[0] - price1[0]) * -1 / (0.001 * T)) + "\n" +
                         "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                         "Error: " + Convert.ToString(price2[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                         time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);

                    }
                    else if (cBMulti.Checked == true && cBControlVar.Checked == false && cBAnti.Checked == true)
                    {
                        price = mult.Pricing(mult.payoff1, type, cv, y);
                        price2 = euro2.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(11);
                        price1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S + 0.001 * S;
                        d1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S - 0.001 * S;
                        d2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S;
                        mult.v = v + v * 0.001;
                        v1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.v = v - v * 0.001;
                        v2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.v = v;
                        mult.T = T + T * 0.001;
                        t1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.T = T;
                        mult.r = r + r * 0.001;
                        r1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.r = r - r * 0.001;
                        r2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(12);
                        mult.r = r;
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price1[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                         "Theta: " + Convert.ToString((t1[0] - price1[0]) * -1 / (0.001 * T)) + "\n" +
                         "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                         "Error: " + Convert.ToString(price2[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                         time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }
                    else if (cBMulti.Checked == true && cBAnti.Checked == false && cBControlVar.Checked == true)
                    {
                        price = mult.Pricing(mult.payoff1, type, cv, y);
                        price2 = euro2.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(11);
                        price1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S + 0.001 * S;
                        d1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S - 0.001 * S;
                        d2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S;
                        mult.v = v + v * 0.001;
                        v1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.v = v - v * 0.001;
                        v2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.v = v;
                        mult.T = T + T * 0.001;
                        t1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.T = T;
                        mult.r = r + r * 0.001;
                        r1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.r = r - r * 0.001;
                        r2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(12);
                        mult.r = r;
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price1[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                         "Theta: " + Convert.ToString((t1[0] - price1[0]) * -1 / (0.001 * T)) + "\n" +
                         "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                         "Error: " + Convert.ToString(price2[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                         time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);

                    }
                    else if (cBMulti.Checked == true && cBAnti.Checked == false && cBControlVar.Checked == false)
                    {
                        price = mult.Pricing(mult.payoff1, type, cv, y);
                        price2 = euro2.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(11);
                        price1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S + 0.001 * S;
                        d1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S - 0.001 * S;
                        d2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S;
                        mult.v = v + v * 0.001;
                        v1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.v = v - v * 0.001;
                        v2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.v = v;
                        mult.T = T + T * 0.001;
                        t1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.T = T;
                        mult.r = r + r * 0.001;
                        r1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.r = r - r * 0.001;
                        r2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(12);
                        mult.r = r;
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price1[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                         "Theta: " + Convert.ToString((t1[0] - price1[0]) * -1 / (0.001 * T)) + "\n" +
                         "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                         "Error: " + Convert.ToString(price2[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                         time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }

                    else
                    {
                        EuroOption euro = new EuroOption();

                        price = euro.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);

                        d1 = euro.Pricing(w, S + S * 0.001, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        d2 = euro.Pricing(w, S - S * 0.001, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        v1 = euro.Pricing(w, S, K, v + v * 0.001, r, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        v2 = euro.Pricing(w, S, K, v - v * 0.001, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        t1 = euro.Pricing(w, S, K, v, r, T + T * 0.001, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        r1 = euro.Pricing(w, S, K, v, r + r * 0.001, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        r2 = euro.Pricing(w, S, K, v, r - r * 0.001, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                        "Theta: " + Convert.ToString((t1[0] - price[0]) * -1 / (0.001 * T)) + "\n" +
                        "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                        "Error: " + Convert.ToString(price[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                        time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }
                }

                else if (rdbPut.Checked == true)
                {
                    int type = 1;
                    mult.type = 1;
                    double[,] w = new double[paths, steps];
                    double[] price, price1, price2, d1, d2, v1, v2, r1, r2, t1 = new double[2];
                    Random1 rnd = new Random1();
                    EuroOption euro1 = new EuroOption();
                    if (cBAnti.Checked == true && cBControlVar.Checked == false)
                    {
                        w = rnd.PolarRejectAnti(paths, steps);
                    }
                    else
                    {
                        w = rnd.PolarReject(paths, steps);
                    }
                    if (cBMulti.Checked == true && cBAnti.Checked == true && cBControlVar.Checked == true)
                    {
                        price = mult.Pricing(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        price2 = euro1.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        price1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S + 0.001 * S;
                        d1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S - 0.001 * S;
                        d2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S;
                        mult.v = v + v * 0.001;
                        v1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.v = v - v * 0.001;
                        v2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.v = v;
                        mult.T = T + T * 0.001;
                        t1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.T = T;
                        mult.r = r + r * 0.001;
                        r1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.r = r - r * 0.001;
                        r2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(12);
                        mult.r = r;
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price1[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                         "Theta: " + Convert.ToString((t1[0] - price1[0]) * -1 / (0.001 * T)) + "\n" +
                         "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                         "Error: " + Convert.ToString(price2[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                         time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);

                    }
                    else if (cBMulti.Checked == true && cv == 0)
                    {
                        price = mult.Pricing(mult.payoff1, type, cv, y);
                        price2 = euro1.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(11);
                        price1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S + 0.001 * S;
                        d1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S - 0.001 * S;
                        d2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S;
                        mult.v = v + v * 0.001;
                        v1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.v = v - v * 0.001;
                        v2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.v = v;
                        mult.T = T + T * 0.001;
                        t1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.T = T;
                        mult.r = r + r * 0.001;
                        r1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.r = r - r * 0.001;
                        r2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(12);
                        mult.r = r;
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price1[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                         "Theta: " + Convert.ToString((t1[0] - price1[0]) * -1 / (0.001 * T)) + "\n" +
                         "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                         "Error: " + Convert.ToString(price2[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                         time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }
                    else if (cBMulti.Checked == true && cBAnti.Checked == true)
                    {
                        price = mult.Pricing(mult.payoff1, type, cv, y);
                        price2 = euro1.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(11);
                        price1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S + 0.001 * S;
                        d1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S - 0.001 * S;
                        d2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S;
                        mult.v = v + v * 0.001;
                        v1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.v = v - v * 0.001;
                        v2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.v = v;
                        mult.T = T + T * 0.001;
                        t1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.T = T;
                        mult.r = r + r * 0.001;
                        r1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.r = r - r * 0.001;
                        r2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(12);
                        mult.r = r;
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price1[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                         "Theta: " + Convert.ToString((t1[0] - price1[0]) * -1 / (0.001 * T)) + "\n" +
                         "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                         "Error: " + Convert.ToString(price2[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                         time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }
                    else if (cBMulti.Checked == true)
                    {
                        price = mult.Pricing(mult.payoff1, type, cv, y);
                        price2 = euro1.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(11);
                        price1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S + 0.001 * S;
                        d1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S - 0.001 * S;
                        d2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.s = S;
                        mult.v = v + v * 0.001;
                        v1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.v = v - v * 0.001;
                        v2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.v = v;
                        mult.T = T + T * 0.001;
                        t1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.T = T;
                        mult.r = r + r * 0.001;
                        r1 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(11);
                        mult.r = r - r * 0.001;
                        r2 = mult.Pricing1(mult.payoff1, type, cv, y);
                        pB1.Increment(12);
                        mult.r = r;
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price1[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                         "Theta: " + Convert.ToString((t1[0] - price1[0]) * -1 / (0.001 * T)) + "\n" +
                         "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                         "Error: " + Convert.ToString(price2[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                         time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);

                    }

                    else
                    {
                        EuroOption euro = new EuroOption();
                        price = euro.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        d1 = euro.Pricing(w, S + S * 0.001, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        d2 = euro.Pricing(w, S - S * 0.001, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        v1 = euro.Pricing(w, S, K, v + v * 0.001, r, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        v2 = euro.Pricing(w, S, K, v - v * 0.001, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        t1 = euro.Pricing(w, S, K, v, r, T + T * 0.001, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        r1 = euro.Pricing(w, S, K, v, r + r * 0.001, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        r2 = euro.Pricing(w, S, K, v, r - r * 0.001, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        time.Stop();

                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                        "Theta: " + Convert.ToString((t1[0] - price[0]) * -1 / (0.001 * T)) + "\n" +
                        "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                        "Error: " + Convert.ToString(price[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                        time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }
                }
                else
                {
                    rTBresults.Text = "Please select a Call or Put option";
                }
            }
            else if (rbasian.Checked == true)
            {
                int type = 0;
                int optype = 0;
                mult1.optype = optype;
                mult1.type = 0;
                double[,] w = new double[paths, steps];
                double[] price, price1, price2, d1, d2, v1, v2, r1, r2, t1 = new double[2];
                Random1 rnd = new Random1();
                Asian euro2 = new Asian();
                if (cBAnti.Checked == true && cBControlVar.Checked == false)
                {
                    w = rnd.PolarRejectAnti(paths, steps);
                }
                else
                {
                    w = rnd.PolarReject(paths, steps);
                }
                if (raBCall.Checked == true)
                {
                    if (cBMulti.Checked == true)
                    {
                        mult1.greek = 0;
                        price = mult1.Pricing(type, cv, y);
                        price2 = euro2.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(11);
                        mult1.greek = 1;
                        price1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.s = S + 0.001 * S;
                        d1 = mult1.Pricing( type, cv, y);
                        pB1.Increment(11);
                        mult1.s = S - 0.001 * S;
                        d2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(12);
                        mult1.s = S;
                        mult1.v = v + v * 0.001;
                        v1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.v = v - v * 0.001;
                        v2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.v = v;
                        mult1.T = T + T * 0.001;
                        t1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.T = T;
                        mult1.r = r + r * 0.001;
                        r1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.r = r - r * 0.001;
                        r2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.r = r;
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price1[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                         "Theta: " + Convert.ToString((t1[0] - price1[0]) * -1 / (0.001 * T)) + "\n" +
                         "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                         "Error: " + Convert.ToString(price2[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                         time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }
                    else
                    {
                        Asian euro = new Asian();

                        price = euro.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);

                        d1 = euro.Pricing(w, S + S * 0.001, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        d2 = euro.Pricing(w, S - S * 0.001, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        v1 = euro.Pricing(w, S, K, v + v * 0.001, r, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        v2 = euro.Pricing(w, S, K, v - v * 0.001, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        t1 = euro.Pricing(w, S, K, v, r, T + T * 0.001, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        r1 = euro.Pricing(w, S, K, v, r + r * 0.001, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        r2 = euro.Pricing(w, S, K, v, r - r * 0.001, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                        "Theta: " + Convert.ToString((t1[0] - price[0]) * -1 / (0.001 * T)) + "\n" +
                        "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                        "Error: " + Convert.ToString(price[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                        time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }
                }
                else if (rdbPut.Checked == true)
                {
                    type = 1;
                    mult1.optype = 1;

                    if (cBMulti.Checked == true)
                    {
                        price = mult1.Pricing(type, cv, y);
                        price2 = euro2.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(11);
                        mult1.greek = 1;
                        price1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.s = S + 0.001 * S;
                        d1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.s = S - 0.001 * S;
                        d2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(12);
                        mult1.s = S;
                        mult1.v = v + v * 0.001;
                        v1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.v = v - v * 0.001;
                        v2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.v = v;
                        mult1.T = T + T * 0.001;
                        t1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.T = T;
                        mult1.r = r + r * 0.001;
                        r1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.r = r - r * 0.001;
                        r2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.r = r;
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price1[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                         "Theta: " + Convert.ToString((t1[0] - price1[0]) * -1 / (0.001 * T)) + "\n" +
                         "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                         "Error: " + Convert.ToString(price2[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                         time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }
                    else
                   
                    {
                        Asian asian = new Asian();

                        price = asian.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);

                        d1 = asian.Pricing(w, S + S * 0.001, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        d2 = asian.Pricing(w, S - S * 0.001, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        v1 = asian.Pricing(w, S, K, v + v * 0.001, r, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        v2 = asian.Pricing(w, S, K, v - v * 0.001, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        t1 = asian.Pricing(w, S, K, v, r, T + T * 0.001, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        r1 = asian.Pricing(w, S, K, v, r + r * 0.001, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        r2 = asian.Pricing(w, S, K, v, r - r * 0.001, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                        "Theta: " + Convert.ToString((t1[0] - price[0]) * -1 / (0.001 * T)) + "\n" +
                        "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                        "Error: " + Convert.ToString(price[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                        time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }

                }
                else
                {
                    rTBresults.Text = "PLease select a Call or Put Option";
                }
            }
            else if (digital.Checked == true)
            {
                int type = 0;
                mult1.type = 0;
                int optype = 1;
                mult1.optype = optype;
                double[,] w = new double[paths, steps];
                double[] price, price1, price2, d1, d2, v1, v2, r1, r2, t1 = new double[2];
                Random1 rnd = new Random1();
                Digital euro2 = new Digital();
                if (cBAnti.Checked == true && cBControlVar.Checked == false)
                {
                    w = rnd.PolarRejectAnti(paths, steps);
                }
                else
                {
                    w = rnd.PolarReject(paths, steps);
                }
                if (raBCall.Checked == true)
                {
                    if (cBMulti.Checked == true)
                    {
                        mult1.greek = 0;
                        price = mult1.Pricing(type, cv, y);
                        price2 = euro2.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv,rebate);
                        pB1.Increment(11);
                        mult1.greek = 1;
                        price1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.s = S + 0.001 * S;
                        d1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.s = S - 0.001 * S;
                        d2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(12);
                        mult1.s = S;
                        mult1.v = v + v * 0.001;
                        v1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.v = v - v * 0.001;
                        v2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.v = v;
                        mult1.T = T + T * 0.001;
                        t1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.T = T;
                        mult1.r = r + r * 0.001;
                        r1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.r = r - r * 0.001;
                        r2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.r = r;
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price1[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                         "Theta: " + Convert.ToString((t1[0] - price1[0]) * -1 / (0.001 * T)) + "\n" +
                         "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                         "Error: " + Convert.ToString(price2[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                         time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }
                    else
                    {
                        Digital euro = new Digital();

                        mult1.greek = 0;
                        price = euro.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv, rebate);
                        pB1.Increment(12);

                        d1 = euro.Pricing(w, S + S * 0.001, K, v, r, T, steps, paths, type, y, cv, rebate);
                        pB1.Increment(13);
                        d2 = euro.Pricing(w, S - S * 0.001, K, v, r, T, steps, paths, type, y, cv, rebate);
                        pB1.Increment(12);
                        v1 = euro.Pricing(w, S, K, v + v * 0.001, r, T, steps, paths, type, y, cv, rebate);
                        pB1.Increment(13);
                        v2 = euro.Pricing(w, S, K, v - v * 0.001, r, T, steps, paths, type, y, cv, rebate);
                        pB1.Increment(12);
                        t1 = euro.Pricing(w, S, K, v, r, T + T * 0.001, steps, paths, type, y, cv, rebate);
                        pB1.Increment(13);
                        r1 = euro.Pricing(w, S, K, v, r + r * 0.001, T, steps, paths, type, y, cv, rebate);
                        pB1.Increment(12);
                        r2 = euro.Pricing(w, S, K, v, r - r * 0.001, T, steps, paths, type, y, cv, rebate);
                        pB1.Increment(13);
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                        "Theta: " + Convert.ToString((t1[0] - price[0]) * -1 / (0.001 * T)) + "\n" +
                        "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                        "Error: " + Convert.ToString(price[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                        time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }
                }
                else if (rdbPut.Checked == true)
                {
                    type = 1;
                    mult1.type = 1;
                    if (cBMulti.Checked == true)
                    {
                        price = mult1.Pricing(type, cv, y);
                        price2 = euro2.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv,rebate);
                        pB1.Increment(11);
                        mult1.greek = 1;
                        price1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.s = S + 0.001 * S;
                        d1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.s = S - 0.001 * S;
                        d2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(12);
                        mult1.s = S;
                        mult1.v = v + v * 0.001;
                        v1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.v = v - v * 0.001;
                        v2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.v = v;
                        mult1.T = T + T * 0.001;
                        t1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.T = T;
                        mult1.r = r + r * 0.001;
                        r1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.r = r - r * 0.001;
                        r2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.r = r;
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price1[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                         "Theta: " + Convert.ToString((t1[0] - price1[0]) * -1 / (0.001 * T)) + "\n" +
                         "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                         "Error: " + Convert.ToString(price2[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                         time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }
                    else 
                    {
                        Digital asian = new Digital();

                        price = asian.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv,rebate);
                        pB1.Increment(12);

                        d1 = asian.Pricing(w, S + S * 0.001, K, v, r, T, steps, paths, type, y, cv, rebate);
                        pB1.Increment(13);
                        d2 = asian.Pricing(w, S - S * 0.001, K, v, r, T, steps, paths, type, y, cv, rebate);
                        pB1.Increment(12);
                        v1 = asian.Pricing(w, S, K, v + v * 0.001, r, T, steps, paths, type, y, cv, rebate);
                        pB1.Increment(13);
                        v2 = asian.Pricing(w, S, K, v - v * 0.001, r, T, steps, paths, type, y, cv, rebate);
                        pB1.Increment(12);
                        t1 = asian.Pricing(w, S, K, v, r, T + T * 0.001, steps, paths, type, y, cv, rebate);
                        pB1.Increment(13);
                        r1 = asian.Pricing(w, S, K, v, r + r * 0.001, T, steps, paths, type, y, cv, rebate);
                        pB1.Increment(12);
                        r2 = asian.Pricing(w, S, K, v, r - r * 0.001, T, steps, paths, type, y, cv, rebate);
                        pB1.Increment(13);
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                        "Theta: " + Convert.ToString((t1[0] - price[0]) * -1 / (0.001 * T)) + "\n" +
                        "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                        "Error: " + Convert.ToString(price[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                        time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }

                }
                else
                {
                    rTBresults.Text = "PLease select a Call or Put Option";
                }
            }
            else if (lookback.Checked == true)
            {
                int type = 0;
                mult1.type = 0;
                int optype = 2;
                mult1.optype = optype;
                double[,] w = new double[paths, steps];
                double[] price, price1, price2, d1, d2, v1, v2, r1, r2, t1 = new double[2];
                Random1 rnd = new Random1();
                LookBack euro2 = new LookBack();
                if (cBAnti.Checked == true && cBControlVar.Checked == false)
                {
                    w = rnd.PolarRejectAnti(paths, steps);
                }
                else
                {
                    w = rnd.PolarReject(paths, steps);
                }
                if (raBCall.Checked == true)
                {
                    if (cBMulti.Checked == true)
                    {
                        mult1.greek = 0;
                        price = mult1.Pricing(type, cv, y);
                        price2 = euro2.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(11);
                        mult1.greek = 1;
                        price1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.s = S + 0.001 * S;
                        d1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.s = S - 0.001 * S;
                        d2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(12);
                        mult1.s = S;
                        mult1.v = v + v * 0.001;
                        v1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.v = v - v * 0.001;
                        v2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.v = v;
                        mult1.T = T + T * 0.001;
                        t1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.T = T;
                        mult1.r = r + r * 0.001;
                        r1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.r = r - r * 0.001;
                        r2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.r = r;
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price1[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                         "Theta: " + Convert.ToString((t1[0] - price1[0]) * -1 / (0.001 * T)) + "\n" +
                         "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                         "Error: " + Convert.ToString(price2[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                         time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }
                    else
                    {
                        LookBack euro = new LookBack();
                        mult1.greek = 0;
                        price = euro.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);

                        d1 = euro.Pricing(w, S + S * 0.001, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        d2 = euro.Pricing(w, S - S * 0.001, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        v1 = euro.Pricing(w, S, K, v + v * 0.001, r, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        v2 = euro.Pricing(w, S, K, v - v * 0.001, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        t1 = euro.Pricing(w, S, K, v, r, T + T * 0.001, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        r1 = euro.Pricing(w, S, K, v, r + r * 0.001, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        r2 = euro.Pricing(w, S, K, v, r - r * 0.001, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                        "Theta: " + Convert.ToString((t1[0] - price[0]) * -1 / (0.001 * T)) + "\n" +
                        "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                        "Error: " + Convert.ToString(price[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                        time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }
                }
                else if (rdbPut.Checked == true)
                {
                    type = 1;
                    mult1.type = 1;
                    if (cBMulti.Checked == true)
                    {
                        price = mult1.Pricing(type, cv, y);
                        price2 = euro2.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(11);
                        mult1.greek = 1;
                        price1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.s = S + 0.001 * S;
                        d1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.s = S - 0.001 * S;
                        d2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(12);
                        mult1.s = S;
                        mult1.v = v + v * 0.001;
                        v1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.v = v - v * 0.001;
                        v2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.v = v;
                        mult1.T = T + T * 0.001;
                        t1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.T = T;
                        mult1.r = r + r * 0.001;
                        r1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.r = r - r * 0.001;
                        r2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.r = r;
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price1[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                         "Theta: " + Convert.ToString((t1[0] - price1[0]) * -1 / (0.001 * T)) + "\n" +
                         "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                         "Error: " + Convert.ToString(price2[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                         time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }
                    else
                    {
                        LookBack asian = new LookBack();

                        price = asian.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);

                        d1 = asian.Pricing(w, S + S * 0.001, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        d2 = asian.Pricing(w, S - S * 0.001, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        v1 = asian.Pricing(w, S, K, v + v * 0.001, r, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        v2 = asian.Pricing(w, S, K, v - v * 0.001, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        t1 = asian.Pricing(w, S, K, v, r, T + T * 0.001, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        r1 = asian.Pricing(w, S, K, v, r + r * 0.001, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        r2 = asian.Pricing(w, S, K, v, r - r * 0.001, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                        "Theta: " + Convert.ToString((t1[0] - price[0]) * -1 / (0.001 * T)) + "\n" +
                        "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                        "Error: " + Convert.ToString(price[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                        time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }

                }
                else
                {
                    rTBresults.Text = "PLease select a Call or Put Option";
                }
            }
            else if (range.Checked == true)
            {
                int type = 0;
                mult1.type = 0;
                int optype = 3;
                mult1.optype = optype;
                double[,] w = new double[paths, steps];
                double[] price, price1, price2, d1, d2, v1, v2, r1, r2, t1 = new double[2];
                Random1 rnd = new Random1();
                Range euro2 = new Range();
                if (cBAnti.Checked == true && cBControlVar.Checked == false)
                {
                    w = rnd.PolarRejectAnti(paths, steps);
                }
                else
                {
                    w = rnd.PolarReject(paths, steps);
                }
                if (raBCall.Checked == true)
                {
                    if (cBMulti.Checked == true)
                    {
                        mult1.greek = 0;
                        price = mult1.Pricing(type, cv, y);
                        price2 = euro2.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(11);
                        mult1.greek = 1;
                        price1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.s = S + 0.001 * S;
                        d1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.s = S - 0.001 * S;
                        d2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(12);
                        mult1.s = S;
                        mult1.v = v + v * 0.001;
                        v1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.v = v - v * 0.001;
                        v2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.v = v;
                        mult1.T = T + T * 0.001;
                        t1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.T = T;
                        mult1.r = r + r * 0.001;
                        r1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.r = r - r * 0.001;
                        r2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.r = r;
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price1[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                         "Theta: " + Convert.ToString((t1[0] - price1[0]) * -1 / (0.001 * T)) + "\n" +
                         "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                         "Error: " + Convert.ToString(price2[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                         time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }
                    else
                    {
                        Range euro = new Range();
                        mult1.greek = 0;
                        price = euro.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);

                        d1 = euro.Pricing(w, S + S * 0.001, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        d2 = euro.Pricing(w, S - S * 0.001, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        v1 = euro.Pricing(w, S, K, v + v * 0.001, r, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        v2 = euro.Pricing(w, S, K, v - v * 0.001, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        t1 = euro.Pricing(w, S, K, v, r, T + T * 0.001, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        r1 = euro.Pricing(w, S, K, v, r + r * 0.001, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        r2 = euro.Pricing(w, S, K, v, r - r * 0.001, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                        "Theta: " + Convert.ToString((t1[0] - price[0]) * -1 / (0.001 * T)) + "\n" +
                        "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                        "Error: " + Convert.ToString(price[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                        time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }
                }
                else if (rdbPut.Checked == true)
                {
                    type = 1;
                    mult1.type = 1;
                    if (cBMulti.Checked == true)
                    {
                        price = mult1.Pricing(type, cv, y);
                        price2 = euro2.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(11);
                        mult1.greek = 1;
                        price1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.s = S + 0.001 * S;
                        d1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.s = S - 0.001 * S;
                        d2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(12);
                        mult1.s = S;
                        mult1.v = v + v * 0.001;
                        v1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.v = v - v * 0.001;
                        v2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.v = v;
                        mult1.T = T + T * 0.001;
                        t1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.T = T;
                        mult1.r = r + r * 0.001;
                        r1 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.r = r - r * 0.001;
                        r2 = mult1.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult1.r = r;
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price1[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                         "Theta: " + Convert.ToString((t1[0] - price1[0]) * -1 / (0.001 * T)) + "\n" +
                         "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                         "Error: " + Convert.ToString(price2[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                         time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }
                    else
                    {
                        Range asian = new Range();

                        price = asian.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);

                        d1 = asian.Pricing(w, S + S * 0.001, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        d2 = asian.Pricing(w, S - S * 0.001, K, v, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        v1 = asian.Pricing(w, S, K, v + v * 0.001, r, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        v2 = asian.Pricing(w, S, K, v - v * 0.001, r, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        t1 = asian.Pricing(w, S, K, v, r, T + T * 0.001, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        r1 = asian.Pricing(w, S, K, v, r + r * 0.001, T, steps, paths, type, y, cv);
                        pB1.Increment(12);
                        r2 = asian.Pricing(w, S, K, v, r - r * 0.001, T, steps, paths, type, y, cv);
                        pB1.Increment(13);
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                        "Theta: " + Convert.ToString((t1[0] - price[0]) * -1 / (0.001 * T)) + "\n" +
                        "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                        "Error: " + Convert.ToString(price[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                        time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }

                }
                else
                {
                    rTBresults.Text = "PLease select a Call or Put Option";
                }
            }
            else if (Barrier.Checked == true)
            {
                int type = 0;
                mult2.type = 0;

                double[,] w = new double[paths, steps];
                double[] price, price1, price2, d1, d2, v1, v2, r1, r2, t1 = new double[2];
                Random1 rnd = new Random1();
                Barrier euro2 = new Barrier();
                if (cBAnti.Checked == true && cBControlVar.Checked == false)
                {
                    w = rnd.PolarRejectAnti(paths, steps);
                }
                else
                {
                    w = rnd.PolarReject(paths, steps);
                }
                if (raBCall.Checked == true)
                {
                    if (cBMulti.Checked == true)
                    {
                        mult2.greek = 0;
                        price = mult2.Pricing(type, cv, y);
                        price2 = euro2.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv, bartype,bar);
                        mult2.greek = 1;
                        pB1.Increment(11);
                        mult2.greek = 1;
                        price1 = mult2.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult2.s = S + 0.001 * S;
                        d1 = mult2.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult2.s = S - 0.001 * S;
                        d2 = mult2.Pricing(type, cv, y);
                        pB1.Increment(12);
                        mult2.s = S;
                        mult2.v = v + v * 0.001;
                        v1 = mult2.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult2.v = v - v * 0.001;
                        v2 = mult2.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult2.v = v;
                        mult2.T = T + T * 0.001;
                        t1 = mult2.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult2.T = T;
                        mult2.r = r + r * 0.001;
                        r1 = mult2.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult2.r = r - r * 0.001;
                        r2 = mult2.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult2.r = r;
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price1[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                         "Theta: " + Convert.ToString((t1[0] - price1[0]) * -1 / (0.001 * T)) + "\n" +
                         "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                         "Error: " + Convert.ToString(price2[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                         time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }
                    else
                    {
                        Barrier euro = new Barrier();

                        mult2.greek = 0;
                        price = euro.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv,bartype,bar);
                        pB1.Increment(12);
                        mult2.greek = 1;
                        d1 = euro.Pricing(w, S + S * 0.001, K, v, r, T, steps, paths, type, y, cv,bartype,bar);
                        pB1.Increment(13);
                        d2 = euro.Pricing(w, S - S * 0.001, K, v, r, T, steps, paths, type, y, cv, bartype, bar);
                        pB1.Increment(12);
                        v1 = euro.Pricing(w, S, K, v + v * 0.001, r, T, steps, paths, type, y, cv, bartype, bar);
                        pB1.Increment(13);
                        v2 = euro.Pricing(w, S, K, v - v * 0.001, r, T, steps, paths, type, y, cv, bartype, bar);
                        pB1.Increment(12);
                        t1 = euro.Pricing(w, S, K, v, r, T + T * 0.001, steps, paths, type, y, cv, bartype, bar);
                        pB1.Increment(13);
                        r1 = euro.Pricing(w, S, K, v, r + r * 0.001, T, steps, paths, type, y, cv, bartype, bar);
                        pB1.Increment(12);
                        r2 = euro.Pricing(w, S, K, v, r - r * 0.001, T, steps, paths, type, y, cv, bartype, bar);
                        pB1.Increment(13);
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                        "Theta: " + Convert.ToString((t1[0] - price[0]) * -1 / (0.001 * T)) + "\n" +
                        "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                        "Error: " + Convert.ToString(price[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                        time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }
                }
                else if (rdbPut.Checked == true)
                {
                    type = 1;
                    mult2.type = 1;
                    if (cBMulti.Checked == true)
                    {
                        mult2.greek = 0;
                        price = mult2.Pricing(type, cv, y);
                        price2 = euro2.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv, bartype,bar);
                        pB1.Increment(11);
                        mult2.greek = 1;
                        price1 = mult2.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult2.s = S + 0.001 * S;
                        d1 = mult2.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult2.s = S - 0.001 * S;
                        d2 = mult2.Pricing(type, cv, y);
                        pB1.Increment(12);
                        mult2.s = S;
                        mult2.v = v + v * 0.001;
                        v1 = mult2.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult2.v = v - v * 0.001;
                        v2 = mult2.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult2.v = v;
                        mult2.T = T + T * 0.001;
                        t1 = mult2.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult2.T = T;
                        mult2.r = r + r * 0.001;
                        r1 = mult2.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult2.r = r - r * 0.001;
                        r2 = mult2.Pricing(type, cv, y);
                        pB1.Increment(11);
                        mult2.r = r;
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price1[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                         "Theta: " + Convert.ToString((t1[0] - price1[0]) * -1 / (0.001 * T)) + "\n" +
                         "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                         "Error: " + Convert.ToString(price2[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                         time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }

                    else
                    {
                        Barrier asian = new Barrier();

                        price = asian.Pricing(w, S, K, v, r, T, steps, paths, type, y, cv, bartype, bar);
                        pB1.Increment(12);
                        mult2.greek = 1;
                        d1 = asian.Pricing(w, S + S * 0.001, K, v, r, T, steps, paths, type, y, cv, bartype, bar);
                        pB1.Increment(13);
                        d2 = asian.Pricing(w, S - S * 0.001, K, v, r, T, steps, paths, type, y, cv, bartype, bar);
                        pB1.Increment(12);
                        v1 = asian.Pricing(w, S, K, v + v * 0.001, r, T, steps, paths, type, y, cv, bartype, bar);
                        pB1.Increment(13);
                        v2 = asian.Pricing(w, S, K, v - v * 0.001, r, T, steps, paths, type, y, cv, bartype, bar);
                        pB1.Increment(12);
                        t1 = asian.Pricing(w, S, K, v, r, T + T * 0.001, steps, paths, type, y, cv, bartype, bar);
                        pB1.Increment(13);
                        r1 = asian.Pricing(w, S, K, v, r + r * 0.001, T, steps, paths, type, y, cv, bartype, bar);
                        pB1.Increment(12);
                        r2 = asian.Pricing(w, S, K, v, r - r * 0.001, T, steps, paths, type, y, cv, bartype, bar);
                        pB1.Increment(13);
                        time.Stop();
                        rTBresults.Text = "Price: " + Convert.ToString(price[0]) + "\n" +
                        "Delta: " + Convert.ToString((d1[0] - d2[0]) / (2 * S * 0.001)) + "\n" +
                        "Gamma: " + Convert.ToString((d1[0] - 2 * price[0] + d2[0]) / (Math.Pow(S * 0.001, 2))) + "\n" +
                        "Vega: " + Convert.ToString((v1[0] - v2[0]) / (2 * 0.001 * v)) + "\n" +
                        "Theta: " + Convert.ToString((t1[0] - price[0]) * -1 / (0.001 * T)) + "\n" +
                        "Rho: " + Convert.ToString((r1[0] - r2[0]) / (2 * 0.001 * r)) + "\n" +
                        "Error: " + Convert.ToString(price[1]) + "\n" + "Time Elapsed: " + time.Elapsed.Hours.ToString() + ":" +
                        time.Elapsed.Minutes.ToString() + ":" + time.Elapsed.Seconds.ToString() + ":" +
                        time.Elapsed.Milliseconds.ToString() + "\n" + "Number of Cores: " + Convert.ToString(mult.cores);
                    }

                }
                else
                {
                    rTBresults.Text = "PLease select a Call or Put Option";
                }
            }
            else
            {
                rTBresults.Text = "PLease Select option type";
            }
        }
    }

    abstract class Option
    {
        public double S { get; set; }
        public double K { get; set; }
        public double V { get; set; }
        public double R { get; set; }
        public double T { get; set; }
    }

    sealed class EuroOption : Option
    {
        public double[] Pricing(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int type, int anti, int cv)
        {
            double vj = 0;
            double er = 0;
            double sumct2 = 0;
            double[] err = new double[(paths + 1) / 2];
            double[] answer = new double[2];
            Simulator sim = new Simulator();
            double[] payoff = new double[paths + 1];

            if (anti == 0 && cv == 0)
            {
                payoff = sim.EuroSimCVAnti(rand, s, k, v, r, T, steps, paths, type);
            }
            else if (cv == 0)
            {
                payoff = sim.EuroSimCV(rand, s, k, v, r, T, steps, paths, type);
            }
            else if (anti == 0)
            {
                payoff = sim.EuroSimAnti(rand, s, k, v, r, T, steps, paths);
            }
            else
            {
                payoff = sim.EuroSim(rand, s, k, v, r, T, steps, paths);
            }
            if (type == 0 && cv != 0)
            {
                for (int i = 0; i < payoff.Length; i++)
                {
                    if (payoff[i] - k > 0)
                    {
                        vj += payoff[i] - k;
                        payoff[i] = payoff[i] - k;
                    }
                    else
                    {
                        vj += 0;
                        payoff[i] = 0;
                    }
                }
            }
            else if (type == 1 && cv != 0)
            {
                for (int i = 0; i < paths; i++)
                {
                    if (k - payoff[i] > 0)
                    {
                        vj += k - payoff[i];
                        payoff[i] = k - payoff[i];
                    }
                    else
                    {
                        vj += 0;
                        payoff[i] = 0;
                    }
                }
            }
            else
            {
                for (int i = 0; i < payoff.Length; i++)
                {
                    vj += payoff[i];
                    sumct2 = sumct2 + payoff[i] * payoff[i];
                }
            }
            answer[0] = (vj / paths) * Math.Pow(Math.E, -r * T);
            if (anti == 1 && cv != 0)
            {
                for (int i = 0; i < paths; i++)
                {
                    er += Math.Pow(((payoff[i] * Math.Pow(Math.E, -r * T)) - answer[0]), 2);
                }
                er = Math.Sqrt(er / (paths - 1));
                er = er / Math.Sqrt(paths);
                answer[1] = er;
            }
            else if (anti == 0)

            {
                double var = 0;
                for (int i = 0; i < (paths + 1) / 2; i++)
                {
                    err[i] = (Math.Pow(Math.E, -r * T) * (payoff[i] + payoff[i + ((paths + 1) / 2)])) / 2;
                    er += err[i];
                }
                er = er / ((paths + 1) / 2);
                for (int i = 0; i < (paths + 1) / 2; i++)
                {
                    var += Math.Pow((err[i] - er), 2);
                }

                var = var / ((paths + 1) / 2);
                var = var / ((paths + 1) / 2);
                var = Math.Sqrt(var);
                answer[1] = var;
            }

            else
            {
                double sd = Math.Sqrt((sumct2 - vj * vj / paths) * Math.Exp(-2 * r * T) / (paths - 1));
                answer[1] = sd / Math.Sqrt(paths);
            }


            return answer;
        }

    }

    sealed class Asian : Option
    {
        public double[] Pricing(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int type, int anti, int cv)
        {
            double vj = 0;
            double er = 0;
            double sumct2 = 0;
            double[] err = new double[(paths + 1) / 2];
            double[] answer = new double[2];
            double[,] payoff = new double[paths + 1, steps];
            double[] payoff2 = new double[paths + 1];
            double steps2 = Convert.ToDouble(steps);
            Simulator sim = new Simulator();

            if (anti == 0 && cv == 0)
            {
                payoff2 = sim.AsianSimCVAnti(rand, s, k, v, r, T, steps, paths, type);

            }
            else if (cv == 0)
            {
                payoff2 = sim.AsianSimCV(rand, s, k, v, r, T, steps, paths, type);
            }
            else if (anti == 0)
            {
                payoff = sim.PathSimAnti(rand, s, k, v, r, T, steps, paths);
            }
            else
            {
                payoff = sim.PathSim(rand, s, k, v, r, T, steps, paths);
            }
            if (cv != 0)
            {
                for (int i = 0; i < paths + 1; i++)
                {
                    for (int j = 0; j < steps; j++)
                    {
                        payoff2[i] += payoff[i, j];
                    }
                    payoff2[i] = payoff2[i] / steps2;
                }
            }


            if (type == 0 && cv != 0)
            {
                for (int i = 0; i < payoff2.Length; i++)
                {
                    if (payoff2[i] - k > 0)
                    {
                        vj += payoff2[i] - k;
                        payoff2[i] = payoff2[i] - k;
                    }
                    else
                    {
                        vj += 0;
                        payoff2[i] = 0;
                    }
                }
            }
            else if (type == 1 && cv != 0)
            {
                for (int i = 0; i < paths; i++)
                {
                    if (k - payoff2[i] > 0)
                    {
                        vj += k - payoff2[i];
                        payoff2[i] = k - payoff2[i];
                    }
                    else
                    {
                        vj += 0;
                        payoff2[i] = 0;
                    }
                }
            }
            else
            {
                for (int i = 0; i < payoff2.Length; i++)
                {
                    vj += payoff2[i];
                    sumct2 = sumct2 + payoff2[i] * payoff2[i];
                }
            }
            answer[0] = (vj / paths) * Math.Pow(Math.E, -r * T);
            if (anti == 1 && cv != 0)
            {
                for (int i = 0; i < paths; i++)
                {
                    er += Math.Pow(((payoff2[i] * Math.Pow(Math.E, -r * T)) - answer[0]), 2);
                }
                er = Math.Sqrt(er / (paths - 1));
                er = er / Math.Sqrt(paths);
                answer[1] = er;
            }
            else if (anti == 0)

            {
                double var = 0;
                for (int i = 0; i < (paths + 1) / 2; i++)
                {
                    err[i] = (Math.Pow(Math.E, -r * T) * (payoff2[i] + payoff2[i + ((paths + 1) / 2)])) / 2;
                    er += err[i];
                }
                er = er / ((paths + 1) / 2);
                for (int i = 0; i < (paths + 1) / 2; i++)
                {
                    var += Math.Pow((err[i] - er), 2);
                }

                var = var / ((paths + 1) / 2);
                var = var / ((paths + 1) / 2);
                var = Math.Sqrt(var);
                answer[1] = var;
            }

            else
            {
                double sd = Math.Sqrt((sumct2 - vj * vj / paths) * Math.Exp(-2 * r * T) / (paths - 1));
                answer[1] = sd / Math.Sqrt(paths);
            }


            return answer;
        }
    }

    sealed class Digital : Option
    {
        public double[] Pricing(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int type, int anti, int cv, double rebate)
        {
            double vj = 0;
            double er = 0;
            double sumct2 = 0;
            double[] err = new double[(paths + 1) / 2];
            double[] answer = new double[2];
            double[,] payoff = new double[paths + 1, steps];
            double[] payoff2 = new double[paths];
            double steps2 = Convert.ToDouble(steps);
            Simulator sim = new Simulator();

            if (anti == 0 && cv == 0)
            {
                payoff2 = sim.DigSimCVAnti(rand, s, k, v, r, T, steps, paths, type, rebate);

            }
            else if (cv == 0)
            {
                payoff2 = sim.DigSimCV(rand, s, k, v, r, T, steps, paths, type, rebate);
            }
            else if (anti == 0)
            {
               payoff2 = sim.EuroSimAnti(rand, s, k, v, r, T, steps, paths);
            }
            else
            {
                payoff2 = sim.EuroSim(rand, s, k, v, r, T, steps, paths);
            }


            if (type == 0 && cv != 0)
            {
                for (int i = 0; i < payoff2.Length; i++)
                {
                    if (payoff2[i] >= k)
                    {
                        vj += rebate;
                        payoff2[i] = rebate;
                    }
                    else
                    {
                        vj += 0;
                        payoff2[i] = 0;
                    }
                }
            }
            else if (type == 1 && cv != 0)
            {
                for (int i = 0; i < paths; i++)
                {
                    if (payoff2[i] <= k)
                    {
                        vj += rebate;
                        payoff2[i] = rebate;
                    }
                    else
                    {
                        vj += 0;
                        payoff2[i] = 0;
                    }
                }
            }
            else
            {
                for (int i = 0; i < payoff2.Length; i++)
                {
                    vj += payoff2[i];
                    sumct2 = sumct2 + payoff2[i] * payoff2[i];
                }
            }
            answer[0] = (vj / paths) * Math.Pow(Math.E, -r * T);
            if (anti == 1 && cv != 0)
            {
                for (int i = 0; i < paths; i++)
                {
                    er += Math.Pow(((payoff2[i] * Math.Pow(Math.E, -r * T)) - answer[0]), 2);
                }
                er = Math.Sqrt(er / (paths - 1));
                er = er / Math.Sqrt(paths);
                answer[1] = er;
            }
            else if (anti == 0)

            {
                double var = 0;
                for (int i = 0; i < (paths + 1) / 2; i++)
                {
                    err[i] = (Math.Pow(Math.E, -r * T) * (payoff2[i] + payoff2[i + ((paths + 1) / 2)])) / 2;
                    er += err[i];
                }
                er = er / ((paths + 1) / 2);
                for (int i = 0; i < (paths + 1) / 2; i++)
                {
                    var += Math.Pow((err[i] - er), 2);
                }

                var = var / ((paths + 1) / 2);
                var = var / ((paths + 1) / 2);
                var = Math.Sqrt(var);
                answer[1] = var;
            }

            else
            {
                double sd = Math.Sqrt((sumct2 - vj * vj / paths) * Math.Exp(-2 * r * T) / (paths - 1));
                answer[1] = sd / Math.Sqrt(paths);
            }


            return answer;
        }
    }
    sealed class Barrier : Option
    {
        public double[] Pricing(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int type, int anti, int cv, int bartype, double bar)
        {
            double vj = 0;
            double er = 0;
            double sumct2 = 0;
            double[] err = new double[(paths + 1) / 2];
            double[] answer = new double[2];
            double[,] payoff = new double[paths + 1, steps];
            double[] payoff2 = new double[paths + 1];
            double[] payoff3 = new double[paths + 1];
            double steps2 = Convert.ToDouble(steps);
            Simulator sim = new Simulator();

            if (anti == 0 && cv == 0)
            {
                payoff2 = sim.BarSimCVAnti(rand, s, k, v, r, T, steps, paths, bartype, bar, type);
            }
            else if (cv == 0)
            {
                payoff2 = sim.BarSimCV(rand, s, k, v, r, T, steps, paths, bartype, bar, type);
            }
            else if (anti == 0)
            {
                payoff2 = sim.BarSimAnti(rand, s, k, v, r, T, steps, paths, bartype, bar, type);
            }
            else
            {
                payoff2 = sim.BarSim(rand, s, k, v, r, T, steps, paths, bartype, bar, type);
            }

            for (int i = 0; i < payoff2.Length; i++)
            {
                vj += payoff2[i];
                sumct2 = sumct2 + payoff2[i] * payoff2[i];
            }

            answer[0] = (vj / paths) * Math.Pow(Math.E, -r * T);
            if (anti == 1 && cv != 0)
            {
                for (int i = 0; i < paths; i++)
                {
                    er += Math.Pow(((payoff2[i] * Math.Pow(Math.E, -r * T)) - answer[0]), 2);
                }
                er = Math.Sqrt(er / (paths - 1));
                er = er / Math.Sqrt(paths);
                answer[1] = er;
            }
            else if (anti == 0)

            {
                double var = 0;
                for (int i = 0; i < (paths + 1) / 2; i++)
                {
                    err[i] = (Math.Pow(Math.E, -r * T) * (payoff2[i] + payoff2[i + ((paths + 1) / 2)])) / 2;
                    er += err[i];
                }
                er = er / ((paths + 1) / 2);
                for (int i = 0; i < (paths + 1) / 2; i++)
                {
                    var += Math.Pow((err[i] - er), 2);
                }

                var = var / ((paths + 1) / 2);
                var = var / ((paths + 1) / 2);
                var = Math.Sqrt(var);
                answer[1] = var;
            }

            else
            {
                double sd = Math.Sqrt((sumct2 - vj * vj / paths) * Math.Exp(-2 * r * T) / (paths - 1));
                answer[1] = sd / Math.Sqrt(paths);
            }


            return answer;
        }
    }
    sealed class LookBack : Option
    {
        public double[] Pricing(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int type, int anti, int cv)
        {
            double vj = 0;
            double er = 0;
            double sumct2 = 0;
            double[] err = new double[(paths + 1) / 2];
            double[] answer = new double[2];
            double[,] payoff = new double[paths + 1, steps];
            double[] payoff2 = new double[paths + 1];
            double steps2 = Convert.ToDouble(steps);
            Simulator sim = new Simulator();

            if (anti == 0 && cv == 0)
            {
                payoff2 = sim.LookSimCVAnti(rand, s, k, v, r, T, steps, paths, type);
            }
            else if (cv == 0)
            {
                payoff2 = sim.LookSimCV(rand, s, k, v, r, T, steps, paths, type);
            }
            else if (anti == 0)
            {
                payoff = sim.PathSimAnti(rand, s, k, v, r, T, steps, paths);
            }
            else
            {
                payoff = sim.PathSim(rand, s, k, v, r, T, steps, paths);
            }

            if (type == 0 && cv != 0)
            {
                for (int i = 0; i < paths + 1; i++)
                {
                    payoff2[i] = s;
                    for (int j = 1; j < steps; j++)
                    {
                        if (payoff[i, j] > payoff2[i])
                        {
                            payoff2[i] = payoff[i, j];
                        }

                    }

                }
            }
            else if (type == 1 && cv != 0)
            {
                for (int i = 0; i < paths + 1; i++)
                {
                    payoff2[i] = s;
                    for (int j = 1; j < steps; j++)
                    {
                        if (payoff[i, j] < payoff2[i])
                        {
                            payoff2[i] = payoff[i, j];
                        }

                    }

                }
            }


            if (type == 0 && cv != 0)
            {
                for (int i = 0; i < payoff2.Length; i++)
                {
                    if (payoff2[i] - k > 0)
                    {
                        vj += payoff2[i] - k;
                        payoff2[i] = payoff2[i] - k;
                    }
                    else
                    {
                        vj += 0;
                        payoff2[i] = 0;
                    }
                }
            }
            else if (type == 1 && cv != 0)
            {
                for (int i = 0; i < payoff2.Length; i++)
                {
                    if (k - payoff2[i] > 0)
                    {
                        vj += k - payoff2[i];
                        payoff2[i] = k - payoff2[i];
                    }
                    else
                    {
                        vj += 0;
                        payoff2[i] = 0;
                    }
                }
            }
            else
            {
                for (int i = 0; i < payoff2.Length; i++)
                {
                    vj += payoff2[i];
                    sumct2 = sumct2 + payoff2[i] * payoff2[i];
                }
            }
            answer[0] = (vj / paths) * Math.Pow(Math.E, -r * T);
            if (anti == 1 && cv != 0)
            {
                for (int i = 0; i < paths; i++)
                {
                    er += Math.Pow(((payoff2[i] * Math.Pow(Math.E, -r * T)) - answer[0]), 2);
                }
                er = Math.Sqrt(er / (paths - 1));
                er = er / Math.Sqrt(paths);
                answer[1] = er;
            }
            else if (anti == 0)

            {
                double var = 0;
                for (int i = 0; i < (paths + 1) / 2; i++)
                {
                    err[i] = (Math.Pow(Math.E, -r * T) * (payoff2[i] + payoff2[i + ((paths + 1) / 2)])) / 2;
                    er += err[i];
                }
                er = er / ((paths + 1) / 2);
                for (int i = 0; i < (paths + 1) / 2; i++)
                {
                    var += Math.Pow((err[i] - er), 2);
                }

                var = var / ((paths + 1) / 2);
                var = var / ((paths + 1) / 2);
                var = Math.Sqrt(var);
                answer[1] = var;
            }

            else
            {
                double sd = Math.Sqrt((sumct2 - vj * vj / paths) * Math.Exp(-2 * r * T) / (paths - 1));
                answer[1] = sd / Math.Sqrt(paths);
            }


            return answer;
        }
    }
    sealed class Range : Option
    {
        public double[] Pricing(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int type, int anti, int cv)
        {
            double vj = 0;
            double er = 0;
            double sumct2 = 0;
            double[] err = new double[(paths + 1) / 2];
            double[] answer = new double[2];
            double[,] payoff = new double[paths + 1, steps];
            double[] payoff2 = new double[paths + 1];
            double[] payoff3 = new double[paths + 1];
            double steps2 = Convert.ToDouble(steps);
            Simulator sim = new Simulator();
            if (anti == 0 && cv == 0)
            {
                payoff2 = sim.RangeSimCVAnti(rand, s, k, v, r, T, steps, paths, type);
            }
            else if (cv == 0)
            {
                payoff2 = sim.RangeSimCV(rand, s, k, v, r, T, steps, paths, type);
            }
            else if (anti == 0)
            {
                payoff = sim.PathSimAnti(rand, s, k, v, r, T, steps, paths);
            }
            else
            {
                payoff = sim.PathSim(rand, s, k, v, r, T, steps, paths);
            }

            if (cv != 0)
            {
                for (int i = 0; i < paths + 1; i++)
                {
                    payoff2[i] = s;
                    for (int j = 1; j < steps; j++)
                    {
                        if (payoff[i, j] > payoff2[i])
                        {
                            payoff2[i] = payoff[i, j];
                        }

                    }

                }

                for (int i = 0; i < paths + 1; i++)
                {
                    payoff3[i] = s;
                    for (int j = 1; j < steps; j++)
                    {
                        if (payoff[i, j] < payoff3[i])
                        {
                            payoff3[i] = payoff[i, j];
                        }

                    }

                }
                for (int i = 0; i < payoff2.Length; i++)
                {

                    vj += payoff2[i] - payoff3[i];
                    payoff2[i] = payoff2[i] - payoff3[i];

                }
            }

            else
            {
                for (int i = 0; i < payoff2.Length; i++)
                {
                    vj += payoff2[i];
                    sumct2 = sumct2 + payoff2[i] * payoff2[i];
                }
            }
            answer[0] = (vj / paths) * Math.Pow(Math.E, -r * T);
            if (anti == 1 && cv != 0)
            {
                for (int i = 0; i < paths; i++)
                {
                    er += Math.Pow(((payoff2[i] * Math.Pow(Math.E, -r * T)) - answer[0]), 2);
                }
                er = Math.Sqrt(er / (paths - 1));
                er = er / Math.Sqrt(paths);
                answer[1] = er;
            }
            else if (anti == 0)

            {
                double var = 0;
                for (int i = 0; i < (paths + 1) / 2; i++)
                {
                    err[i] = (Math.Pow(Math.E, -r * T) * (payoff2[i] + payoff2[i + ((paths + 1) / 2)])) / 2;
                    er += err[i];
                }
                er = er / ((paths + 1) / 2);
                for (int i = 0; i < (paths + 1) / 2; i++)
                {
                    var += Math.Pow((err[i] - er), 2);
                }

                var = var / ((paths + 1) / 2);
                var = var / ((paths + 1) / 2);
                var = Math.Sqrt(var);
                answer[1] = var;
            }

            else
            {
                double sd = Math.Sqrt((sumct2 - vj * vj / paths) * Math.Exp(-2 * r * T) / (paths - 1));
                answer[1] = sd / Math.Sqrt(paths);
            }


            return answer;
        }
    }
    public class Random1
    {

        // Create the Polar Rejection Method for turning uniformally distributed RV's to normally distributed RV's
        public double[,] PolarReject(int p, int s)
        {
            //Declare Variables

            double[,] prand = new double[p + 1, s];
            double rand;
            double rand2;
            double w;
            double c;
            Random rnd = new Random();
            // Create a for loop that creates a matrix of random numbers
            for (int a = 0; a < s; a++)
            {
                for (int b = 0; b <= p - 1; b += 2)
                {

                    // Create a do while function that will refind rand and rand2 if w>1
                    do
                    {
                        rand = 2 * rnd.NextDouble() - 1;
                        rand2 = 2 * rnd.NextDouble() - 1;
                        w = rand * rand + rand2 * rand2;
                    }
                    while (w > 1);
                    //Run the arithmatic to get normally distributed RV's
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    prand[b, a] = c * rand;
                    prand[b + 1, a] = c * rand2;

                }
            }
            //Return the matrix of doubles 
            return prand;
        }
        // Create the Polar Rejection Method for turning uniformally distributed RV's to normally distributed RV's
        public double[,] PolarRejectAnti(int p, int s)
        {
            //Declare Variables
            double[,] prand = new double[(p + 3) / 2, s];
            double rand;
            double rand2;
            double w;
            double c;
            Random rnd = new Random();
            // Create a for loop that creates a matrix of random numbers
            for (int a = 0; a < s; a++)
            {
                for (int b = 0; b <= (p - 1) / 2; b += 2)
                {

                    // Create a do while function that will refind rand and rand2 if w>1
                    do
                    {
                        rand = 2 * rnd.NextDouble() - 1;
                        rand2 = 2 * rnd.NextDouble() - 1;
                        w = rand * rand + rand2 * rand2;
                    }
                    while (w > 1);
                    //Run the arithmatic to get normally distributed RV's
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    prand[b, a] = c * rand;
                    prand[b + 1, a] = c * rand2;

                }
            }
            //Return the matrix of doubles 
            return prand;
        }
    }

    public class Simulator
    {

        public double[,] PathSim(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths)
        {
            double[,] payoff = new double[paths+1, steps];
            double steps1 = Convert.ToDouble(steps);
            double t = T / steps1;
            for (int i = 0; i < paths+1; i++)
            {
                payoff[i, 0] = s;
            }

            for (int j = 0; j < paths+1; j++)
            {
                for (int i = 0; i < steps - 1; i++)
                {
                    payoff[j, i + 1] = payoff[j, i] * Math.Pow(Math.E, ((r - ((v * v) / 2)) * t) + ((v * Math.Sqrt(t)) * rand[j, i]));
                }
            }
            return payoff;
        }
        public double[] BarSim(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int bartype, double bar, int type)
        {
            double[,] payoff = new double[paths + 1, steps];
            double[] payoff2 = new double[paths + 1];
            double steps1 = Convert.ToDouble(steps);
            double t = T / steps1;
            for (int i = 0; i < paths + 1; i++)
            {
                payoff[i, 0] = s;
            }

            for (int j = 0; j < paths; j++)
            {
                double max = s;
                double min = s;
                for (int i = 0; i < steps - 1; i++)
                {
                    payoff[j, i + 1] = payoff[j, i] * Math.Pow(Math.E, ((r - ((v * v) / 2)) * t) + ((v * Math.Sqrt(t)) * rand[j, i]));
                    if (max < payoff[j, i + 1])
                    {
                        max = payoff[j, i + 1];
                    }
                    if (min > payoff[j, i + 1])
                    {
                        min = payoff[j, i + 1];
                    }
                }
                if (bartype == 0 && max >= bar)
                {
                    if (type == 0)
                    {
                        payoff2[j] = Math.Max(payoff[j, steps - 1] - k, 0);
                    }
                    else if (type == 1)
                    {
                        payoff2[j] = Math.Max(k - payoff[j, steps - 1], 0);

                    }

                }
                else if (bartype == 0)
                {
                    payoff2[j] = 0;
                }
                else if (bartype == 1 && max >= bar)
                {
                    payoff2[j] = 0;
                }
                else if (bartype == 1)
                {
                    if (type == 0)
                    {
                        payoff2[j] = Math.Max(payoff[j, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff2[j] = Math.Max(k - payoff[j, steps - 1], 0);
                    }
                }
                else if (bartype == 2 && min <= bar)
                {
                    if (type == 0)
                    {
                        payoff2[j] = Math.Max(payoff[j, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff2[j] = Math.Max(k - payoff[j, steps - 1], 0);
                    }
                }
                else if (bartype == 2)
                {
                    payoff2[j] = 0;
                }
                else if (bartype == 3 && min <= bar)
                {
                    payoff2[j] = 0;
                }
                else
                {
                    if (type == 0)
                    {
                        payoff2[j] = Math.Max(payoff[j, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff2[j] = Math.Max(k - payoff[j, steps - 1], 0);
                    }
                }
            }
            return payoff2;
        }
        public double[,] PathSimAnti(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths)
        {
            double[,] payoff2 = new double[paths + 2, steps];
            double steps1 = Convert.ToDouble(steps);
            double t = T / steps1;

            for (int i = 0; i < paths + 2; i++)
            {

                payoff2[i, 0] = s;
            }

            for (int i = 0; i < (paths + 1) / 2; i++)
            {
                for (int j = 0; j < steps - 1; j++)
                {
                    payoff2[i, j + 1] = payoff2[i, j] * Math.Pow(Math.E, ((r - ((v * v) / 2)) * t) + ((v * Math.Sqrt(t)) * rand[i, j]));
                    payoff2[i + (paths + 1) / 2, j + 1] = payoff2[i + (paths + 1) / 2, j] * Math.Pow(Math.E, ((r - ((v * v) / 2)) * t) + ((v * Math.Sqrt(t)) * (-1 * rand[i, j])));

                }
            }



            return payoff2;


        }
        public double[] BarSimAnti(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int bartype, double bar, int type)
        {
            double[,] payoff2 = new double[paths + 2, steps];
            double[] payoff = new double[paths + 1];
            double steps1 = Convert.ToDouble(steps);
            double t = T / steps1;


            for (int i = 0; i < paths + 2; i++)
            {

                payoff2[i, 0] = s;
            }

            for (int i = 0; i < (paths + 1) / 2; i++)
            {
                double max1 = s;
                double max2 = s;
                double min1 = s;
                double min2 = s;
                for (int j = 0; j < steps - 1; j++)
                {
                    payoff2[i, j + 1] = payoff2[i, j] * Math.Pow(Math.E, ((r - ((v * v) / 2)) * t) + ((v * Math.Sqrt(t)) * rand[i, j]));
                    payoff2[i + (paths + 1) / 2, j + 1] = payoff2[i + (paths + 1) / 2, j] * Math.Pow(Math.E, ((r - ((v * v) / 2)) * t) + ((v * Math.Sqrt(t)) * (-1 * rand[i, j])));
                    if (max1 < payoff2[i, j + 1])
                    {
                        max1 = payoff2[i, j + 1];
                    }
                    if (min1 > payoff2[i, j + 1])
                    {
                        min1 = payoff2[i, j + 1];
                    }
                    if (max2 < payoff2[i + (paths + 1) / 2, j + 1])
                    {
                        max2 = payoff2[i + (paths + 1) / 2, j + 1];
                    }
                    if (min2 > payoff2[i + (paths + 1) / 2, j + 1])
                    {
                        min2 = payoff2[i + (paths + 1) / 2, j + 1];
                    }
                }
                if (bartype == 0 && max1 >= bar)
                {
                    if (type == 0)
                    {
                        payoff[i] = Math.Max(payoff2[i, steps - 1] - k, 0);
                    }
                    else if (type == 1)
                    {
                        payoff[i] = Math.Max(k - payoff2[i, steps - 1], 0);

                    }

                }
                else if (bartype == 0)
                {
                    payoff[i] = 0;
                }
                else if (bartype == 1 && max1 >= bar)
                {
                    payoff[i] = 0;
                }
                else if (bartype == 1)
                {
                    if (type == 0)
                    {
                        payoff[i] = Math.Max(payoff2[i, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff[i] = Math.Max(k - payoff2[i, steps - 1], 0);
                    }
                }
                else if (bartype == 2 && min1 <= bar)
                {
                    if (type == 0)
                    {
                        payoff[i] = Math.Max(payoff2[i, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff[i] = Math.Max(k - payoff2[i, steps - 1], 0);
                    }
                }
                else if (bartype == 2)
                {
                    payoff[i] = 0;
                }
                else if (bartype == 3 && min1 <= bar)
                {
                    payoff[i] = 0;
                }
                else
                {
                    if (type == 0)
                    {
                        payoff[i] = Math.Max(payoff2[i, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff[i] = Math.Max(k - payoff2[i, steps - 1], 0);
                    }
                }

                if (bartype == 0 && max2 >= bar)
                {
                    if (type == 0)
                    {
                        payoff[i + (paths + 1) / 2] = Math.Max(payoff2[i + (paths + 1) / 2, steps - 1] - k, 0);
                    }
                    else if (type == 1)
                    {
                        payoff[i + (paths + 1) / 2] = Math.Max(k - payoff2[i + (paths + 1) / 2, steps - 1], 0);

                    }

                }
                else if (bartype == 0)
                {
                    payoff[i + (paths + 1) / 2] = 0;
                }
                else if (bartype == 1 && max2 >= bar)
                {
                    payoff[i + (paths + 1) / 2] = 0;
                }
                else if (bartype == 1)
                {
                    if (type == 0)
                    {
                        payoff[i + (paths + 1) / 2] = Math.Max(payoff2[i + (paths + 1) / 2, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff[i + (paths + 1) / 2] = Math.Max(k - payoff2[i + (paths + 1) / 2, steps - 1], 0);
                    }
                }
                else if (bartype == 2 && min2 <= bar)
                {
                    if (type == 0)
                    {
                        payoff[i + (paths + 1) / 2] = Math.Max(payoff2[i + (paths + 1) / 2, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff[i + (paths + 1) / 2] = Math.Max(k - payoff2[i + (paths + 1) / 2, steps - 1], 0);
                    }
                }
                else if (bartype == 2)
                {
                    payoff[i + (paths + 1) / 2] = 0;
                }
                else if (bartype == 3 && min2 <= bar)
                {
                    payoff[i + (paths + 1) / 2] = 0;
                }
                else
                {
                    if (type == 0)
                    {
                        payoff[i + (paths + 1) / 2] = Math.Max(payoff2[i + (paths + 1) / 2, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff[i + (paths + 1) / 2] = Math.Max(k - payoff2[i + (paths + 1) / 2, steps - 1], 0);
                    }
                }
            }



            return payoff;


        }
        public double[] BarSimCV(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int bartype, double bar, int type)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            double[,] delta = new double[paths + 2, steps + 1];
            double[,] payoff = new double[paths + 2, steps + 1];
            Delta d = new Delta();
            double[] payoff2 = new double[paths];
            for (int i = 0; i < paths; i++)
            {
                payoff[i, 0] = s;
            }

            for (int i = 0; i < paths; i++)
            {
                double cv = 0;
                double max = s;
                double min = s;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta[i, j + 1] = d.Deltacalc(payoff[i, j], k, v, r, T, t, type);
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    cv = cv + delta[i, j + 1] * (payoff[i, j + 1] - payoff[i, j] * Math.Exp(r * dt));
                    if (payoff[i, j + 1] > max)
                    {
                        max = payoff[i, j + 1];
                    }
                    else if (payoff[i, j + 1] < min)
                    {
                        min = payoff[i, j + 1];
                    }

                }
                //Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)
                if (bartype == 0 && max >= bar)
                {
                    if (type == 0)
                    {
                        payoff2[i] = Math.Max(payoff[i, steps - 1] - k, 0) - cv;
                    }
                    else if (type == 1)
                    {
                        payoff2[i] = Math.Max(k - payoff[i, steps - 1], 0) - cv;

                    }

                }
                else if (bartype == 0)
                {
                    payoff2[i] = 0;
                }
                else if (bartype == 1 && max >= bar)
                {
                    payoff2[i] = 0;
                }
                else if (bartype == 1)
                {
                    if (type == 0)
                    {
                        payoff2[i] = Math.Max(payoff[i, steps - 1] - k, 0) - cv;
                    }
                    else
                    {
                        payoff2[i] = Math.Max(k - payoff[i, steps - 1], 0) - cv;
                    }
                }
                else if (bartype == 2 && min <= bar)
                {
                    if (type == 0)
                    {
                        payoff2[i] = Math.Max(payoff[i, steps - 1] - k, 0) - cv;
                    }
                    else
                    {
                        payoff2[i] = Math.Max(k - payoff[i, steps - 1], 0) - cv;
                    }
                }
                else if (bartype == 2)
                {
                    payoff2[i] = 0;
                }
                else if (bartype == 3 && min <= bar)
                {
                    payoff2[i] = 0;
                }
                else
                {
                    if (type == 0)
                    {
                        payoff2[i] = Math.Max(payoff[i, steps - 1] - k, 0) - cv;
                    }
                    else
                    {
                        payoff2[i] = Math.Max(k - payoff[i, steps - 1], 0) - cv;
                    }
                }
            }

            return payoff2;
        }
        public double[] LookSimCV(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int type)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            Delta d = new Delta();
            double[,] delta = new double[paths + 2, steps + 1];
            double[,] payoff = new double[paths + 2, steps + 1];
            double[] paycv = new double[paths];
            for (int i = 0; i < paths; i++)
            {
                payoff[i, 0] = s;
            }

            for (int i = 0; i < paths; i++)
            {
                double cv = 0;
                double max = 0;
                double min = s;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta[i, j + 1] = d.Deltacalc(payoff[i, j], k, v, r, T, t, type);
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    cv = cv + delta[i, j + 1] * (payoff[i, j + 1] - payoff[i, j] * Math.Exp(r * dt));
                    if (type == 0 && payoff[i, j + 1] > max)
                    {
                        max = payoff[i, j + 1];
                    }
                    else if (type == 1 && payoff[i, j + 1] < min)
                    {
                        min = payoff[i, j + 1];
                    }

                }
                //Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)
                if (type == 0)
                {
                    paycv[i] = Math.Max(max - k, 0) + -1 * cv;
                }
                else
                {
                    paycv[i] = Math.Max(k - min, 0) + -1 * cv;
                }

            }

            return paycv;
        }
        public double[] AsianSimCV(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int type)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            Delta d = new Delta();
            double[,] delta = new double[paths + 2, steps + 1];
            double[,] payoff = new double[paths + 2, steps + 1];
            double[] paycv = new double[paths];
            for (int i = 0; i < paths; i++)
            {
                payoff[i, 0] = s;
            }

            for (int i = 0; i < paths; i++)
            {
                double cv = 0;
                double avg = s;


                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta[i, j + 1] = d.Deltacalc(payoff[i, j], k, v, r, T, t, type);
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    cv = cv + delta[i, j + 1] * (payoff[i, j + 1] - payoff[i, j] * Math.Exp(r * dt));
                    avg += payoff[i, j + 1];
                }
                //Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)
                avg = avg / steps1;
                if (type == 0)
                {
                    paycv[i] = Math.Max(avg - k, 0) + -1 * cv;
                }
                else
                {
                    paycv[i] = Math.Max(k - avg, 0) + -1 * cv;
                }


            }

            return paycv;
        }
        public double[] RangeSimCV(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int type)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            Delta d = new Delta();
            double[,] delta = new double[paths + 2, steps + 1];
            double[,] payoff = new double[paths + 2, steps + 1];
            double[] paycv = new double[paths];
            for (int i = 0; i < paths; i++)
            {
                payoff[i, 0] = s;
            }

            for (int i = 0; i < paths; i++)
            {
                double cv = 0;
                double max = 0;
                double min = s;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta[i, j + 1] = d.Deltacalc(payoff[i, j], k, v, r, T, t, type);
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    cv = cv + delta[i, j + 1] * (payoff[i, j + 1] - payoff[i, j] * Math.Exp(r * dt));
                    if (payoff[i, j + 1] > max)
                    {
                        max = payoff[i, j + 1];
                    }
                    if (payoff[i, j + 1] < min)
                    {
                        min = payoff[i, j + 1];
                    }

                }
                //Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)

                paycv[i] = (max - min) + -1 * cv;


            }

            return paycv;
        }
        public double[] DigSimCV(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int type, double rebate)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            Delta d = new Delta();
            double[,] delta = new double[paths + 2, steps + 1];
            double[,] payoff = new double[paths + 2, steps + 1];
            double[] paycv = new double[paths];
            for (int i = 0; i < paths; i++)
            {
                payoff[i, 0] = s;
            }

            for (int i = 0; i < paths; i++)
            {
                double cv = 0;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta[i, j + 1] = d.Deltacalc(payoff[i, j], k, v, r, T, t, type);
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    cv = cv + delta[i, j + 1] * (payoff[i, j + 1] - payoff[i, j] * Math.Exp(r * dt));

                }
                //Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)

                if (type == 0 && payoff[i, steps] >= k)
                {
                    paycv[i] = rebate - cv;
                }
                else if (type == 1 && payoff[i, steps] <= k)
                {
                    paycv[i] = rebate - cv;
                }
                else
                {
                    paycv[i] = 0 - cv;
                }


            }

            return paycv;
        }

        public double[] DigSimCVAnti(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int type, double rebate)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            Delta d = new Delta();
            double[,] delta1 = new double[(paths + 1) / 2, steps + 1];
            double[,] delta2 = new double[(paths + 1) / 2, steps + 1];
            double[,] payoff1 = new double[(paths + 1) / 2, steps + 1];
            double[,] payoff2 = new double[(paths + 1) / 2, steps + 1];
            double[] paycv = new double[paths];
            for (int i = 0; i < (paths + 1) / 2; i++)
            {
                payoff1[i, 0] = s;
                payoff2[i, 0] = s;
            }

            for (int i = 0; i < (paths + 1) / 2; i++)
            {
                double cv1 = 0;
                double cv2 = 0;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta1[i, j + 1] = d.Deltacalc(payoff1[i, j], k, v, r, T, t, type);
                    delta2[i, j + 1] = d.Deltacalc(payoff2[i, j], k, v, r, T, t, type);
                    payoff1[i, j + 1] = payoff1[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    payoff2[i, j + 1] = payoff2[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * (-1 * rand[i, j]));
                    cv1 = cv1 + delta1[i, j + 1] * (payoff1[i, j + 1] - payoff1[i, j] * Math.Exp(r * dt));
                    cv2 = cv2 + delta2[i, j + 1] * (payoff2[i, j + 1] - payoff2[i, j] * Math.Exp(r * dt));
                }
                // Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)

                if (type == 0 && payoff1[i, steps] >= k && payoff2[i, steps] >= k)
                {
                    paycv[i] = (rebate - cv1 + rebate - cv2);
                }
                else if (type == 0 && payoff1[i, steps] < k && payoff2[i, steps] < k)
                {
                    paycv[i] = 0 - cv1 - cv2;
                }
                else if (type == 0)
                {
                    paycv[i] = rebate - cv1 - cv2;
                }
                else if (type == 1 && payoff1[i, steps] <= k && payoff2[i, steps] <= k)
                {
                    paycv[i] = (rebate - cv1 + rebate - cv2);
                }
                else if (type == 0 && payoff1[i, steps] > k && payoff2[i, steps] > k)
                {
                    paycv[i] = 0 - cv1 - cv2;
                }
                else
                {
                    paycv[i] = rebate - cv1 - cv2;
                }

            }

            return paycv;
        }
        public double[] RangeSimCVAnti(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int type)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            Delta d = new Delta();
            double[,] delta1 = new double[(paths + 1) / 2, steps + 1];
            double[,] delta2 = new double[(paths + 1) / 2, steps + 1];
            double[,] payoff1 = new double[(paths + 1) / 2, steps + 1];
            double[,] payoff2 = new double[(paths + 1) / 2, steps + 1];
            double[] paycv = new double[paths];
            for (int i = 0; i < (paths + 1) / 2; i++)
            {
                payoff1[i, 0] = s;
                payoff2[i, 0] = s;
            }

            for (int i = 0; i < (paths + 1) / 2; i++)
            {
                double cv1 = 0;
                double cv2 = 0;
                double max1 = 0;
                double max2 = 0;
                double min1 = s;
                double min2 = s;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta1[i, j + 1] = d.Deltacalc(payoff1[i, j], k, v, r, T, t, type);
                    delta2[i, j + 1] = d.Deltacalc(payoff2[i, j], k, v, r, T, t, type);
                    payoff1[i, j + 1] = payoff1[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    payoff2[i, j + 1] = payoff2[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * (-1 * rand[i, j]));
                    cv1 = cv1 + delta1[i, j + 1] * (payoff1[i, j + 1] - payoff1[i, j] * Math.Exp(r * dt));
                    cv2 = cv2 + delta2[i, j + 1] * (payoff2[i, j + 1] - payoff2[i, j] * Math.Exp(r * dt));

                    if (payoff1[i, j + 1] > max1)
                    {
                        max1 = payoff1[i, j + 1];
                    }
                    if (payoff1[i, j + 1] < min1)
                    {
                        min1 = payoff1[i, j + 1];
                    }
                    if (payoff2[i, j + 1] > max2)
                    {
                        max2 = payoff2[i, j + 1];
                    }
                    if (payoff2[i, j + 1] < min2)
                    {
                        min2 = payoff2[i, j + 1];
                    }
                }
                // Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)

                paycv[i] = (max1 - min1 + -1 * cv1 + max2 - min2 + -1 * cv2);



            }

            return paycv;
        }
        public double[] BarSimCVAnti(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int bartype, double bar, int type)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            Delta d = new Delta();
            double[,] delta1 = new double[(paths + 1) / 2, steps + 1];
            double[,] delta2 = new double[(paths + 1) / 2, steps + 1];
            double[,] payoff1 = new double[(paths + 1) / 2, steps + 1];
            double[,] payoff2 = new double[(paths + 1) / 2, steps + 1];
            double[] paycv = new double[paths];
            for (int i = 0; i < (paths + 1) / 2; i++)
            {
                payoff1[i, 0] = s;
                payoff2[i, 0] = s;
            }

            for (int i = 0; i < (paths + 1) / 2; i++)
            {
                double cv1 = 0;
                double cv2 = 0;
                double max1 = 0;
                double max2 = 0;
                double min1 = s;
                double min2 = s;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta1[i, j + 1] = d.Deltacalc(payoff1[i, j], k, v, r, T, t, type);
                    delta2[i, j + 1] = d.Deltacalc(payoff2[i, j], k, v, r, T, t, type);
                    payoff1[i, j + 1] = payoff1[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    payoff2[i, j + 1] = payoff2[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * (-1 * rand[i, j]));
                    cv1 = cv1 + delta1[i, j + 1] * (payoff1[i, j + 1] - payoff1[i, j] * Math.Exp(r * dt));
                    cv2 = cv2 + delta2[i, j + 1] * (payoff2[i, j + 1] - payoff2[i, j] * Math.Exp(r * dt));

                    if (payoff1[i, j + 1] > max1)
                    {
                        max1 = payoff1[i, j + 1];
                    }
                    else if (payoff1[i, j + 1] < min1)
                    {
                        min1 = payoff1[i, j + 1];
                    }
                    if (payoff2[i, j + 1] > max2)
                    {
                        max2 = payoff2[i, j + 1];
                    }
                    else if (payoff2[i, j + 1] < min2)
                    {
                        min2 = payoff2[i, j + 1];
                    }
                }
                //Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)
                if (bartype == 0 && max1 >= bar && max2 >= bar)
                {
                    if (type == 0)
                    {
                        paycv[i] = Math.Max(payoff1[i, steps - 1] - k, 0) - cv1 + Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
                    }
                    else if (type == 1)
                    {
                        paycv[i] = Math.Max(k - payoff1[i, steps - 1], 0) - cv1 + Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

                    }

                }
                else if (bartype == 0 && max1 >= bar && max2 < bar)
                {
                    if (type == 0)
                    {
                        paycv[i] = Math.Max(payoff1[i, steps - 1] - k, 0) - cv1;
                    }
                    else if (type == 1)
                    {
                        paycv[i] = Math.Max(k - payoff1[i, steps - 1], 0) - cv1;

                    }
                }
                else if (bartype == 0 && max1 < bar && max2 >= bar)
                {
                    if (type == 0)
                    {
                        paycv[i] = Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
                    }
                    else if (type == 1)
                    {
                        paycv[i] = Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

                    }
                }
                else if (bartype == 0)
                {
                    paycv[i] = 0;
                }
                else if (bartype == 1 && max1 >= bar && max2 >= bar)
                {
                    paycv[i] = 0;
                }
                else if (bartype == 1 && max1 >= bar && max2 < bar)
                {
                    if (type == 0)
                    {
                        paycv[i] = Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
                    }
                    else if (type == 1)
                    {
                        paycv[i] = Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

                    }
                }
                else if (bartype == 1 && max1 < bar && max2 >= bar)
                {
                    if (type == 0)
                    {
                        paycv[i] = Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
                    }
                    else if (type == 1)
                    {
                        paycv[i] = Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

                    }
                }
                else if (bartype == 1)
                {
                    if (type == 0)
                    {
                        paycv[i] = Math.Max(payoff1[i, steps - 1] - k, 0) - cv1 + Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
                    }
                    else if (type == 1)
                    {
                        paycv[i] = Math.Max(k - payoff1[i, steps - 1], 0) - cv1 + Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

                    }

                }
                if (bartype == 2 && min1 >= bar && min2 >= bar)
                {
                    paycv[1] = 0;

                }
                else if (bartype == 2 && min1 >= bar && min2 < bar)
                {
                    if (type == 0)
                    {
                        paycv[i] = Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
                    }
                    else if (type == 1)
                    {
                        paycv[i] = Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

                    }
                }
                else if (bartype == 2 && min1 < bar && min2 >= bar)
                {
                    if (type == 0)
                    {
                        paycv[i] = Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
                    }
                    else if (type == 1)
                    {
                        paycv[i] = Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

                    }
                }
                else if (bartype == 2)
                {
                    if (type == 0)
                    {
                        paycv[i] = Math.Max(payoff1[i, steps - 1] - k, 0) - cv1 + Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
                    }
                    else if (type == 1)
                    {
                        paycv[i] = Math.Max(k - payoff1[i, steps - 1], 0) - cv1 + Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

                    }

                }
                if (bartype == 3 && min1 >= bar && min2 >= bar)
                {
                    if (type == 0)
                    {
                        paycv[i] = Math.Max(payoff1[i, steps - 1] - k, 0) - cv1 + Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
                    }
                    else if (type == 1)
                    {
                        paycv[i] = Math.Max(k - payoff1[i, steps - 1], 0) - cv1 + Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

                    }

                }
                else if (bartype == 3 && min1 >= bar && min2 < bar)
                {
                    if (type == 0)
                    {
                        paycv[i] = Math.Max(payoff1[i, steps - 1] - k, 0) - cv1;
                    }
                    else if (type == 1)
                    {
                        paycv[i] = Math.Max(k - payoff1[i, steps - 1], 0) - cv1;

                    }
                }
                else if (bartype == 3 && min1 < bar && min2 >= bar)
                {
                    if (type == 0)
                    {
                        paycv[i] = Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
                    }
                    else if (type == 1)
                    {
                        paycv[i] = Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

                    }
                }
                else if (bartype == 3)
                {
                    paycv[i] = 0;
                }

            }

            return paycv;
        }
        public double[] LookSimCVAnti(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int type)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            Delta d = new Delta();
            double[,] delta1 = new double[(paths + 1) / 2, steps + 1];
            double[,] delta2 = new double[(paths + 1) / 2, steps + 1];
            double[,] payoff1 = new double[(paths + 1) / 2, steps + 1];
            double[,] payoff2 = new double[(paths + 1) / 2, steps + 1];
            double[] paycv = new double[paths];
            for (int i = 0; i < (paths + 1) / 2; i++)
            {
                payoff1[i, 0] = s;
                payoff2[i, 0] = s;
            }

            for (int i = 0; i < (paths + 1) / 2; i++)
            {
                double cv1 = 0;
                double cv2 = 0;
                double max1 = 0;
                double max2 = 0;
                double min1 = s;
                double min2 = s;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta1[i, j + 1] = d.Deltacalc(payoff1[i, j], k, v, r, T, t, type);
                    delta2[i, j + 1] =d.Deltacalc(payoff2[i, j], k, v, r, T, t, type);
                    payoff1[i, j + 1] = payoff1[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    payoff2[i, j + 1] = payoff2[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * (-1 * rand[i, j]));
                    cv1 = cv1 + delta1[i, j + 1] * (payoff1[i, j + 1] - payoff1[i, j] * Math.Exp(r * dt));
                    cv2 = cv2 + delta2[i, j + 1] * (payoff2[i, j + 1] - payoff2[i, j] * Math.Exp(r * dt));

                    if (type == 0 && payoff1[i, j + 1] > max1)
                    {
                        max1 = payoff1[i, j + 1];
                    }
                    else if (type == 1 && payoff1[i, j + 1] < min1)
                    {
                        min1 = payoff1[i, j + 1];
                    }
                    if (type == 0 && payoff2[i, j + 1] > max2)
                    {
                        max2 = payoff2[i, j + 1];
                    }
                    else if (type == 1 && payoff2[i, j + 1] < min2)
                    {
                        min2 = payoff2[i, j + 1];
                    }
                }
                // Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)

                if (type == 0)
                {
                    paycv[i] = (Math.Max(max1 - k, 0) + -1 * cv1 + Math.Max(max2 - k, 0) + -1 * cv2);
                }
                else
                {
                    paycv[i] = (Math.Max(k - min1, 0) + -1 * cv1 + Math.Max(k - min2, 0) + -1 * cv2);
                }

            }

            return paycv;
        }
        public double[] AsianSimCVAnti(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int type)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            Delta d = new Delta();
            double[,] delta1 = new double[(paths + 1) / 2, steps + 1];
            double[,] delta2 = new double[(paths + 1) / 2, steps + 1];
            double[,] payoff1 = new double[(paths + 1) / 2, steps + 1];
            double[,] payoff2 = new double[(paths + 1) / 2, steps + 1];
            double[] paycv = new double[paths];
            for (int i = 0; i < (paths + 1) / 2; i++)
            {
                payoff1[i, 0] = s;
                payoff2[i, 0] = s;
            }

            for (int i = 0; i < (paths + 1) / 2; i++)
            {
                double cv1 = 0;
                double cv2 = 0;
                double avg1 = s;
                double avg2 = s;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta1[i, j + 1] = d.Deltacalc(payoff1[i, j], k, v, r, T, t, type);
                    delta2[i, j + 1] = d.Deltacalc(payoff2[i, j], k, v, r, T, t, type);
                    payoff1[i, j + 1] = payoff1[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    payoff2[i, j + 1] = payoff2[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * (-1 * rand[i, j]));
                    cv1 = cv1 + delta1[i, j + 1] * (payoff1[i, j + 1] - payoff1[i, j] * Math.Exp(r * dt));
                    cv2 = cv2 + delta2[i, j + 1] * (payoff2[i, j + 1] - payoff2[i, j] * Math.Exp(r * dt));
                    avg1 += payoff1[i, j + 1];
                    avg2 += payoff2[i, j + 1];
                }
                // Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)
                avg1 = avg1 / steps1;
                avg2 = avg2 / steps1;
                if (type == 0)
                {
                    paycv[i] = (Math.Max(avg1 - k, 0) + -1 * cv1 + Math.Max(avg2 - k, 0) + -1 * cv2);
                }
                else
                {
                    paycv[i] = (Math.Max(k - avg1, 0) + -1 * cv1 + Math.Max(k - avg2, 0) + -1 * cv2);
                }

            }

            return paycv;
        }
        public double[] EuroSim(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths)
        {
            double[] payoff = new double[paths];
            double steps1 = Convert.ToDouble(steps);
            double t = T / steps1;
            double[,] sum = new double[paths + 4, steps];

            for (int i = 0; i < paths; i++)
            {
                for (int j = 0; j < steps; j++)
                {
                    sum[i, j] = rand[i, j];
                }
            }

            for (int i = 0; i < paths; i++)
            {
                for (int j = 0; j < steps - 1; j++)
                {
                    sum[i, j + 1] = sum[i, j + 1] + sum[i, j];
                }
            }


            for (int i = 0; i < paths; i++)
            {
                payoff[i] = s * Math.Pow(Math.E, ((r - ((v * v) / 2)) * T) + ((v * Math.Sqrt(t)) * sum[i, steps - 1]));
            }

            return payoff;
        }

        public double[] EuroSimAnti(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths)
        {
            double[] payoff = new double[paths + 1];
            double[] payoff1 = new double[(paths + 1) / 2];
            double[] payoff2 = new double[(paths + 1) / 2];
            double steps1 = Convert.ToDouble(steps);
            double t = T / steps1;
            double[,] sum = new double[paths + 1, steps];

            for (int i = 0; i < (paths + 1) / 2; i++)
            {
                for (int j = 0; j < steps; j++)
                {
                    sum[i, j] = rand[i, j];
                }
            }

            for (int i = 0; i < (paths + 1) / 2; i++)
            {
                for (int j = 0; j < steps - 1; j++)
                {
                    sum[i, j + 1] = sum[i, j + 1] + sum[i, j];
                }
            }


            for (int i = 0; i < (paths + 1) / 2; i++)
            {
                payoff1[i] = s * Math.Pow(Math.E, ((r - ((v * v) / 2)) * T) + ((v * Math.Sqrt(t)) * sum[i, steps - 1]));
            }
            for (int i = 0; i < (paths + 1) / 2; i++)
            {
                payoff2[i] = s * Math.Pow(Math.E, ((r - ((v * v) / 2)) * T) + ((v * Math.Sqrt(t)) * (-1 * sum[i, steps - 1])));
            }

            Array.Copy(payoff1, payoff, payoff1.Length);
            Array.Copy(payoff2, 0, payoff, payoff1.Length, payoff2.Length);
            return payoff;
        }

        public double[] EuroSimCV(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int type)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            Delta d = new Delta();
            double[,] delta = new double[paths + 2, steps + 1];
            double[,] payoff = new double[paths + 2, steps + 1];
            double[] paycv = new double[paths];
            for (int i = 0; i < paths; i++)
            {
                payoff[i, 0] = s;
            }

            for (int i = 0; i < paths; i++)
            {
                double cv = 0;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta[i, j + 1] = d.Deltacalc(payoff[i, j], k, v, r, T, t, type);
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    cv = cv + delta[i, j + 1] * (payoff[i, j + 1] - payoff[i, j] * Math.Exp(r * dt));
                }
                //Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)
                if (type == 0)
                {
                    paycv[i] = Math.Max(payoff[i, steps] - k, 0) + -1 * cv;
                }
                else
                {
                    paycv[i] = Math.Max(k - payoff[i, steps], 0) + -1 * cv;
                }

            }

            return paycv;
        }
        public double[] EuroSimCVAnti(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int type)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            Delta d = new Delta();
            double[,] delta1 = new double[(paths + 1) / 2, steps + 1];
            double[,] delta2 = new double[(paths + 1) / 2, steps + 1];
            double[,] payoff1 = new double[(paths + 1) / 2, steps + 1];
            double[,] payoff2 = new double[(paths + 1) / 2, steps + 1];
            double[] paycv = new double[paths];
            for (int i = 0; i < (paths + 1) / 2; i++)
            {
                payoff1[i, 0] = s;
                payoff2[i, 0] = s;
            }

            for (int i = 0; i < (paths + 1) / 2; i++)
            {
                double cv1 = 0;
                double cv2 = 0;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta1[i, j + 1] = d.Deltacalc(payoff1[i, j], k, v, r, T, t, type);
                    delta2[i, j + 1] = d.Deltacalc(payoff2[i, j], k, v, r, T, t, type);
                    payoff1[i, j + 1] = payoff1[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    payoff2[i, j + 1] = payoff2[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * (-1 * rand[i, j]));
                    cv1 = cv1 + delta1[i, j + 1] * (payoff1[i, j + 1] - payoff1[i, j] * Math.Exp(r * dt));
                    cv2 = cv2 + delta2[i, j + 1] * (payoff2[i, j + 1] - payoff2[i, j] * Math.Exp(r * dt));
                }
                // Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)
                if (type == 0)
                {
                    paycv[i] = (Math.Max(payoff1[i, steps] - k, 0) + -1 * cv1 + Math.Max(payoff2[i, steps] - k, 0) + -1 * cv2);
                }
                else
                {
                    paycv[i] = (Math.Max(k - payoff1[i, steps], 0) + -1 * cv1 + Math.Max(k - payoff2[i, steps], 0) + -1 * cv2);
                }

            }

            return paycv;
        }
    }


    public class Delta
    {
        public double Deltacalc(double s, double k, double v, double r, double T, double t, int type)
        {
            double d1;
            double delta;
            d1 = (Math.Log(s / k) + (r + (Math.Pow(v, 2) / 2)) * (T - t)) / (v * Math.Sqrt(T - t));
            if (type == 0)
            {
                delta = Phi(d1);
            }
            else
            {
                delta = Phi(d1) - 1;
            }
            return delta;
        }


        //This is a cdf normal distribution function that I got from https://www.johndcook.com/blog/csharp_phi/ . I tested it against the cdf norm function in
        // Matlab and it passed the tests. 
        static double Phi(double x)
        {
            // constants
            double a1 = 0.254829592;
            double a2 = -0.284496736;
            double a3 = 1.421413741;
            double a4 = -1.453152027;
            double a5 = 1.061405429;
            double p = 0.3275911;

            // Save the sign of x
            int sign = 1;
            if (x < 0)
                sign = -1;
            x = Math.Abs(x) / Math.Sqrt(2.0);

            // A&S formula 7.1.26
            double t = 1.0 / (1.0 + p * x);
            double y = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.Exp(-x * x);

            return 0.5 * (1.0 + sign * y);
        }
    }

    public class MultiCore
    {

        public double s = 50;
        public double k = 50;
        public double v = .5;
        public double r = 0.05;
        public double T = 1;
        public int paths = 20;
        public int steps = 2;
        public int cores = 4;
        public int w = 0;
        public int numco;
        public int type;
        public int greek;
        public double[] payoff1 = new double[10000000];


        public double[] Pricing(double[] payoff, int type, int cv, int anti)
        {
            greek = 0;
            if (cv == 0 && anti == 0)
            {
                Thread[] newthread = new Thread[cores];
                for (int j = 0; j < cores; j++)
                {
                    newthread[j] = new Thread(EuroSimCVAnti);
                    newthread[j].Start(j);

                }
                for (int i = 0; i < cores; i++)
                {
                    newthread[i].Join();
                }
            }
            else if (cv == 0)
            {
                Thread[] newthread = new Thread[cores];
                for (int j = 0; j < cores; j++)
                {
                    newthread[j] = new Thread(EuroSimCV);
                    newthread[j].Start(j);

                }
                for (int i = 0; i < cores; i++)
                {
                    newthread[i].Join();
                }
            }
            else if (anti == 0)
            {
                Thread[] newthread = new Thread[cores];
                for (int j = 0; j < cores; j++)
                {
                    newthread[j] = new Thread(EuroSimAnti);
                    newthread[j].Start(j);

                }
                for (int i = 0; i < cores; i++)
                {
                    newthread[i].Join();
                }
            }
            else
            {
                Thread[] newthread = new Thread[cores];
                for (int j = 0; j < cores; j++)
                 {
                newthread[j] = new Thread(EuroSim);
                newthread[j].Start(j);

                 }
               for (int i = 0; i < cores; i++)
                 {
                newthread[i].Join();
                 }
            }
      

            double vj = 0;
            double er = 0;
            double sumct2 = 0;
            double[] err = new double[(paths + 1) / 2];
            double[] answer = new double[2];
            Simulator sim = new Simulator();

            if (type == 0 && cv != 0)
            {
                for (int i = 0; i < payoff.Length; i++)
                {
                    if (payoff[i] - k > 0)
                    {
                        vj += payoff[i] - k;
                        payoff[i] = payoff[i] - k;
                    }
                    else
                    {
                        vj += 0;
                        payoff[i] = 0;
                    }
                }
            }
            else if (type == 1 && cv != 0)
            {
                for (int i = 0; i < paths; i++)
                {
                    if (k - payoff[i] > 0)
                    {
                        vj += k - payoff[i];
                        payoff[i] = k - payoff[i];
                    }
                    else
                    {
                        vj += 0;
                        payoff[i] = 0;
                    }
                }
            }
            else
            {
                for (int i = 0; i < payoff.Length; i++)
                {
                    vj += payoff[i];
                    sumct2 = sumct2 + payoff[i] * payoff[i];
                }
            }
            answer[0] = (vj / paths) * Math.Pow(Math.E, -r * T);
            if (anti == 1 && cv != 0)
            {
                for (int i = 0; i < paths; i++)
                {
                    er += Math.Pow(((payoff[i] * Math.Pow(Math.E, -r * T)) - answer[0]), 2);
                }
                er = Math.Sqrt(er / (paths - 1));
                er = er / Math.Sqrt(paths);
                answer[1] = er;
            }
            else if (anti == 0)

            {
                double var = 0;
                for (int i = 0; i < (paths + 1) / 2; i++)
                {
                    err[i] = (Math.Pow(Math.E, -r * T) * (payoff[i] + payoff[i + ((paths + 1) / 2)])) / 2;
                    er += err[i];
                }
                er = er / ((paths + 1) / 2);
                for (int i = 0; i < (paths + 1) / 2; i++)
                {
                    var += Math.Pow((err[i] - er), 2);
                }

                var = var / ((paths + 1) / 2);
                var = var / ((paths + 1) / 2);
                var = Math.Sqrt(var);
                answer[1] = var;
            }

            else
            {
                double sd = Math.Sqrt((sumct2 - vj * vj / paths) * Math.Exp(-2 * r * T) / (paths - 1));
                answer[1] = sd / Math.Sqrt(paths);
            }


            return answer;

        }
        public double[] Pricing1(double[] payoff, int type, int cv, int anti)
        {
            greek = 1;
            if (cv == 0 && anti == 0)
            {
                Thread[] newthread = new Thread[cores];
                for (int j = 0; j < cores; j++)
                {
                    newthread[j] = new Thread(EuroSimCVAnti);
                    newthread[j].Start(j);

                }
                for (int i = 0; i < cores; i++)
                {
                    newthread[i].Join();
                }
            }
            else if (cv == 0)
            {
                Thread[] newthread = new Thread[cores];
                for (int j = 0; j < cores; j++)
                {
                    newthread[j] = new Thread(EuroSimCV);
                    newthread[j].Start(j);

                }
                for (int i = 0; i < cores; i++)
                {
                    newthread[i].Join();
                }
            }
            else if (anti == 0)
            {
                Thread[] newthread = new Thread[cores];
                for (int j = 0; j < cores; j++)
                {
                    newthread[j] = new Thread(EuroSimAnti);
                    newthread[j].Start(j);

                }
                for (int i = 0; i < cores; i++)
                {
                    newthread[i].Join();
                }
            }
            else
            {
                Thread[] newthread = new Thread[cores];
                for (int j = 0; j < cores; j++)
                {
                    newthread[j] = new Thread(EuroSim);
                    newthread[j].Start(j);

                }
                for (int i = 0; i < cores; i++)
                {
                    newthread[i].Join();
                }
            }


            double vj = 0;
            double er = 0;
            double sumct2 = 0;
            double[] err = new double[(paths + 1) / 2];
            double[] answer = new double[2];
            Simulator sim = new Simulator();

            if (type == 0 && cv != 0)
            {
                for (int i = 0; i < payoff.Length; i++)
                {
                    if (payoff[i] - k > 0)
                    {
                        vj += payoff[i] - k;
                        payoff[i] = payoff[i] - k;
                    }
                    else
                    {
                        vj += 0;
                        payoff[i] = 0;
                    }
                }
            }
            else if (type == 1 && cv != 0)
            {
                for (int i = 0; i < paths; i++)
                {
                    if (k - payoff[i] > 0)
                    {
                        vj += k - payoff[i];
                        payoff[i] = k - payoff[i];
                    }
                    else
                    {
                        vj += 0;
                        payoff[i] = 0;
                    }
                }
            }
            else
            {
                for (int i = 0; i < payoff.Length; i++)
                {
                    vj += payoff[i];
                    sumct2 = sumct2 + payoff[i] * payoff[i];
                }
            }
            answer[0] = (vj / paths) * Math.Pow(Math.E, -r * T);
            if (anti == 1 && cv != 0)
            {
                for (int i = 0; i < paths; i++)
                {
                    er += Math.Pow(((payoff[i] * Math.Pow(Math.E, -r * T)) - answer[0]), 2);
                }
                er = Math.Sqrt(er / (paths - 1));
                er = er / Math.Sqrt(paths);
                answer[1] = er;
            }
            else if (anti == 0)

            {
                double var = 0;
                for (int i = 0; i < (paths + 1) / 2; i++)
                {
                    err[i] = (Math.Pow(Math.E, -r * T) * (payoff[i] + payoff[i + ((paths + 1) / 2)])) / 2;
                    er += err[i];
                }
                er = er / ((paths + 1) / 2);
                for (int i = 0; i < (paths + 1) / 2; i++)
                {
                    var += Math.Pow((err[i] - er), 2);
                }

                var = var / ((paths + 1) / 2);
                var = var / ((paths + 1) / 2);
                var = Math.Sqrt(var);
                answer[1] = var;
            }

            else if (cv == 0)
            {
                double sd = Math.Sqrt((sumct2 - vj * vj / paths) * Math.Exp(-2 * r * T) / (paths - 1));
                answer[1] = sd / Math.Sqrt(paths);
            }
            else
            {
                double sd = Math.Sqrt((sumct2 - vj * vj / paths) * Math.Exp(-2 * r * T) / (paths - 1));
                answer[1] = sd / Math.Sqrt(paths);
            }


            return answer;

        }
            public void EuroSim(object l)
        {
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[] payoff = new double[w];
            double steps1 = Convert.ToDouble(steps);
            double t = T / steps1;
            double[,] sum = new double[w + 4, steps];
            double[,] rand = new double[w + 4, steps];
            if (greek == 0)
            {
                rand = PolarReject(w + 1, steps, x);
            }
            else
            {
                rand = PolarReject1(w + 1, steps,x);
            }

            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < steps; j++)
                {
                    sum[i, j] = rand[i, j];
                }
            }

            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < steps - 1; j++)
                {
                    sum[i, j + 1] = sum[i, j + 1] + sum[i, j];
                }
            }


            for (int i = 0; i < w; i++)
            {
                payoff[i] = s * Math.Pow(Math.E, ((r - ((v * v) / 2)) * T) + ((v * Math.Sqrt(t)) * sum[i, steps - 1]));
            }
            payoff.CopyTo(payoff1, x * w);
        }
        public void EuroSimAnti(object l)
        {
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[] payoff3 = new double[(w + 1) / 2];
            double[] payoff2 = new double[(w + 1) / 2];
            double steps1 = Convert.ToDouble(steps);
            double t = T / steps1;
            double[,] sum = new double[w + 1, steps];
            double[,] rand = new double[w + 4, steps];
            if (greek == 0)
            {
                rand = PolarRejectAnti(w + 1, steps, x);
            }
            else
            {
                rand = PolarRejectAnti1(w + 1, steps,x);
            }

            for (int i = 0; i < (w + 1) / 2; i++)
            {
                for (int j = 0; j < steps; j++)
                {
                    sum[i, j] = rand[i, j];
                }
            }

            for (int i = 0; i < (w + 1) / 2; i++)
            {
                for (int j = 0; j < steps - 1; j++)
                {
                    sum[i, j + 1] = sum[i, j + 1] + sum[i, j];
                }
            }


            for (int i = 0; i < (w + 1) / 2; i++)
            {
                payoff3[i] = s * Math.Pow(Math.E, ((r - ((v * v) / 2)) * T) + ((v * Math.Sqrt(t)) * sum[i, steps - 1]));
            }
            for (int i = 0; i < (w + 1) / 2; i++)
            {
                payoff2[i] = s * Math.Pow(Math.E, ((r - ((v * v) / 2)) * T) + ((v * Math.Sqrt(t)) * (-1 * sum[i, steps - 1])));
            }

            payoff3.CopyTo(payoff1, x * w);
            Array.Copy(payoff2,0, payoff1, (2*x+1) * payoff3.Length  , payoff2.Length);
        }
        public void EuroSimCV(object l)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[,] delta = new double[w + 2, steps + 1];
            double[,] payoff = new double[w + 2, steps + 1];
            double[] paycv = new double[w];
            double[,] rand = new double[w + 4, steps];
            if (greek == 0)
            {
                rand = PolarReject(w + 1, steps, x);
            }
            else
            {
                rand = PolarReject2(w + 1, steps, x);
            }
            double[] temp = new double[w + 2];
            for (int i = 0; i < w; i++)
            {
                payoff[i, 0] = s;
            }

            for (int i = 0; i < w; i++)
            {
                double cv1 = 0;

                for (int j = 0; j < steps; j++)
                {
                    t = j * dt;
                    delta[i, j + 1] = Deltacalc(payoff[i, j], k, v, r, T, t, type);
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    cv1 = cv1 + delta[i, j + 1] * (payoff[i, j + 1] - payoff[i, j] * Math.Exp(r * dt));
                }
                //Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)
                if (type == 0)
                {
                    paycv[i] = Math.Max(payoff[i, steps] - k, 0) + -1 * cv1;
                }
                else
                {
                    paycv[i] = Math.Max(k - payoff[i, steps], 0) + -1 * cv1;
                }
            }
            paycv.CopyTo(payoff1, w * x);

        }
        public void EuroSimCVAnti(object l)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[,] delta1 = new double[(w + 1) / 2, steps + 1];
            double[,] delta2 = new double[(w + 1) / 2, steps + 1];
            double[,] payoff = new double[(w + 1) / 2, steps + 1];
            double[,] payoff2 = new double[(w + 1) / 2, steps + 1];
            double[] paycv = new double[w];
            double[,] rand = new double[w + 4, steps];
            if (greek == 0)
            {
                 rand = PolarRejectAnti(w + 1, steps, x);
            }
            else
            {
                rand = PolarRejectAnti1(w + 1, steps, x);
            }
            
            for (int i = 0; i < (w + 1) / 2; i++)
            {
                payoff[i, 0] = s;
                payoff2[i, 0] = s;
            }

            for (int i = 0; i < (w + 1) / 2; i++)
            {
                double cv1 = 0;
                double cv2 = 0;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta1[i, j + 1] = Deltacalc(payoff[i, j], k, v, r, T, t, type);
                    delta2[i, j + 1] = Deltacalc(payoff2[i, j], k, v, r, T, t, type);
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    payoff2[i, j + 1] = payoff2[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * (-1 * rand[i, j]));
                    cv1 = cv1 + delta1[i, j + 1] * (payoff[i, j + 1] - payoff[i, j] * Math.Exp(r * dt));
                    cv2 = cv2 + delta2[i, j + 1] * (payoff2[i, j + 1] - payoff2[i, j] * Math.Exp(r * dt));
                }
                // Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)
                if (type == 0)
                {
                    paycv[i] = (Math.Max(payoff[i, steps] - k, 0) + -1 * cv1 + Math.Max(payoff2[i, steps] - k, 0) + -1 * cv2);
                }
                else
                {
                    paycv[i] = (Math.Max(k - payoff[i, steps], 0) + -1 * cv1 + Math.Max(k - payoff2[i, steps], 0) + -1 * cv2);
                }

            }

            paycv.CopyTo(payoff1, w * x);
        }
        public double[,] PolarReject(int p, int s, int x)
        {
            //Declare Variables

            double[,] prand = new double[p + 1, s];
            double rand;
            double rand2;
            double w;
            double c;

             Random v = new Random();
            double rd = v.NextDouble() * 1000;
            Random rnd = new Random(x * 200 * Convert.ToInt32(rd));
            

           
            // Create a for loop that creates a matrix of random numbers
            for (int a = 0; a < s; a++)
            {
                for (int b = 0; b <= p - 1; b += 2)
                {

                    // Create a do while function that will refind rand and rand2 if w>1
                    do
                    {
                        rand = 2 * rnd.NextDouble() - 1;
                        rand2 = 2 * rnd.NextDouble() - 1;
                        w = rand * rand + rand2 * rand2;
                    }
                    while (w > 1);
                    //Run the arithmatic to get normally distributed RV's
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    prand[b, a] = c * rand;
                    prand[b + 1, a] = c * rand2;

                }
            }
            //Return the matrix of doubles 
            return prand;
        }
        public double[,] PolarRejectAnti(int p, int s, int x)
        {
            //Declare Variables
            double[,] prand = new double[(p + 3) / 2, s];
            double rand;
            double rand2;
            double w;
            double c;

                Random v = new Random();
                double rd = v.NextDouble() * 1000;
                Random rnd = new Random(x * 200 * Convert.ToInt32(rd));
            
            // Create a for loop that creates a matrix of random numbers
            for (int a = 0; a < s; a++)
            {
                for (int b = 0; b <= (p - 1) / 2; b += 2)
                {

                    // Create a do while function that will refind rand and rand2 if w>1
                    do
                    {
                        rand = 2 * rnd.NextDouble() - 1;
                        rand2 = 2 * rnd.NextDouble() - 1;
                        w = rand * rand + rand2 * rand2;
                    }
                    while (w > 1);
                    //Run the arithmatic to get normally distributed RV's
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    prand[b, a] = c * rand;
                    prand[b + 1, a] = c * rand2;

                }
            }
            //Return the matrix of doubles 
            return prand;
        }
        public double[,] PolarReject1(int p, int s, int x)
        {
            //Declare Variables

            double[,] prand = new double[p + 1, s];
            double rand;
            double rand2;
            double w;
            double c;


            Random rnd = new Random(x * 43);


            // Create a for loop that creates a matrix of random numbers
            for (int a = 0; a < s; a++)
            {
                for (int b = 0; b <= p - 1; b += 2)
                {

                    // Create a do while function that will refind rand and rand2 if w>1
                    do
                    {
                        rand = 2 * rnd.NextDouble() - 1;
                        rand2 = 2 * rnd.NextDouble() - 1;
                        w = rand * rand + rand2 * rand2;
                    }
                    while (w > 1);
                    //Run the arithmatic to get normally distributed RV's
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    prand[b, a] = c * rand;
                    prand[b + 1, a] = c * rand2;

                }
            }
            //Return the matrix of doubles 
            return prand;
        }
        public double[,] PolarReject2(int p, int s, int x)
        {
            //Declare Variables

            double[,] prand = new double[p + 1, s];
            double rand;
            double rand2;
            double w;
            double c;


            Random rnd = new Random(43);


            // Create a for loop that creates a matrix of random numbers
            for (int a = 0; a < s; a++)
            {
                for (int b = 0; b <= p - 1; b += 2)
                {

                    // Create a do while function that will refind rand and rand2 if w>1
                    do
                    {
                        rand = 2 * rnd.NextDouble() - 1;
                        rand2 = 2 * rnd.NextDouble() - 1;
                        w = rand * rand + rand2 * rand2;
                    }
                    while (w > 1);
                    //Run the arithmatic to get normally distributed RV's
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    prand[b, a] = c * rand;
                    prand[b + 1, a] = c * rand2;

                }
            }
            //Return the matrix of doubles 
            return prand;
        }
        public double[,] PolarRejectAnti1(int p, int s, int x)
        {
            //Declare Variables
            double[,] prand = new double[(p + 3) / 2, s];
            double rand;
            double rand2;
            double w;
            double c;
            Random rnd = new Random(x * 43);

            // Create a for loop that creates a matrix of random numbers
            for (int a = 0; a < s; a++)
            {
                for (int b = 0; b <= (p - 1) / 2; b += 2)
                {

                    // Create a do while function that will refind rand and rand2 if w>1
                    do
                    {
                        rand = 2 * rnd.NextDouble() - 1;
                        rand2 = 2 * rnd.NextDouble() - 1;
                        w = rand * rand + rand2 * rand2;
                    }
                    while (w > 1);
                    //Run the arithmatic to get normally distributed RV's
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    prand[b, a] = c * rand;
                    prand[b + 1, a] = c * rand2;

                }
            }
            //Return the matrix of doubles 
            return prand;
        }
        public double Deltacalc(double s, double k, double v, double r, double T, double t, int type)
        {
            double d1;
            double delta;
            d1 = (Math.Log(s / k) + (r + (Math.Pow(v, 2) / 2)) * (T - t)) / (v * Math.Sqrt(T - t));
            if (type == 0)
            {
                delta = Phi(d1);
            }
            else
            {
                delta = Phi(d1) - 1;
            }
            return delta;
        }


        //This is a cdf normal distribution function that I got from https://www.johndcook.com/blog/csharp_phi/ . I tested it against the cdf norm function in
        // Matlab and it passed the tests. 
        static double Phi(double x)
        {
            // constants
            double a1 = 0.254829592;
            double a2 = -0.284496736;
            double a3 = 1.421413741;
            double a4 = -1.453152027;
            double a5 = 1.061405429;
            double p = 0.3275911;

            // Save the sign of x
            int sign = 1;
            if (x < 0)
                sign = -1;
            x = Math.Abs(x) / Math.Sqrt(2.0);

            // A&S formula 7.1.26
            double t = 1.0 / (1.0 + p * x);
            double y = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.Exp(-x * x);

            return 0.5 * (1.0 + sign * y);
        }

        //Works Cited
        // Cook, John. "Stand-alone C# code for Φ(x)." https://www.johndcook.com/blog/csharp_phi/. Accessed 5 March 2018.

    }
    public class MultiCore1
    {

        public double s = 50;
        public double k = 50;
        public double v = .5;
        public double r = 0.05;
        public double T = 1;
        public int paths = 20;
        public int steps = 2;
        public int cores = 4;
        public int w = 0;
        public int numco;
        public int type;
        public int greek = 0;
        public int optype;
        public double rebate = 5;
        public double[] payoff1 = new double[10000000];
        public double[,] payoff2 = new double[1000000, 252];


        public double[] Pricing(int type, int cv, int anti)
        {
           
            double[] payoff = new double[paths + 2];
            if(optype == 0)
            {
                if (cv == 0 && anti == 0)
                {
                    Thread[] newthread = new Thread[cores];
                    for (int j = 0; j < cores; j++)
                    {
                        newthread[j] = new Thread(AsianSimCVAnti);
                        newthread[j].Start(j);

                    }
                    for (int i = 0; i < cores; i++)
                    {
                        newthread[i].Join();
                    }
                }
                else if (cv == 0)
                {
                    Thread[] newthread = new Thread[cores];
                    for (int j = 0; j < cores; j++)
                    {
                        newthread[j] = new Thread(AsianSimCV);
                        newthread[j].Start(j);

                    }
                    for (int i = 0; i < cores; i++)
                    {
                        newthread[i].Join();
                    }
                }
                else if (anti == 0)
                {
                    Thread[] newthread = new Thread[cores];
                    for (int j = 0; j < cores; j++)
                    {
                        newthread[j] = new Thread(PathSimAnti);
                        newthread[j].Start(j);

                    }
                    for (int i = 0; i < cores; i++)
                    {
                        newthread[i].Join();
                    }
                }
                else
                {
                    Thread[] newthread = new Thread[cores];
                    for (int j = 0; j < cores; j++)
                    {
                        newthread[j] = new Thread(PathSim);
                        newthread[j].Start(j);

                    }
                    for (int i = 0; i < cores; i++)
                    {
                        newthread[i].Join();
                    }
                }

            }
            else if (optype == 1)
            {
                if (cv == 0 && anti == 0)
                {
                    Thread[] newthread = new Thread[cores];
                    for (int j = 0; j < cores; j++)
                    {
                        newthread[j] = new Thread(DigSimCVAnti);
                        newthread[j].Start(j);

                    }
                    for (int i = 0; i < cores; i++)
                    {
                        newthread[i].Join();
                    }
                }
                else if (cv == 0)
                {
                    Thread[] newthread = new Thread[cores];
                    for (int j = 0; j < cores; j++)
                    {
                        newthread[j] = new Thread(DigSimCV);
                        newthread[j].Start(j);

                    }
                    for (int i = 0; i < cores; i++)
                    {
                        newthread[i].Join();
                    }
                }
                else if (anti == 0)
                {
                    Thread[] newthread = new Thread[cores];
                    for (int j = 0; j < cores; j++)
                    {
                        newthread[j] = new Thread(EuroSimAnti);
                        newthread[j].Start(j);

                    }
                    for (int i = 0; i < cores; i++)
                    {
                        newthread[i].Join();
                    }
                }
                else
                {
                    Thread[] newthread = new Thread[cores];
                    for (int j = 0; j < cores; j++)
                    {
                        newthread[j] = new Thread(EuroSim);
                        newthread[j].Start(j);

                    }
                    for (int i = 0; i < cores; i++)
                    {
                        newthread[i].Join();
                    }
                }

            }
            else if (optype == 2)
            {
                if (cv == 0 && anti == 0)
                {
                    Thread[] newthread = new Thread[cores];
                    for (int j = 0; j < cores; j++)
                    {
                        newthread[j] = new Thread(LookSimCVAnti);
                        newthread[j].Start(j);

                    }
                    for (int i = 0; i < cores; i++)
                    {
                        newthread[i].Join();
                    }
                }
                else if (cv == 0)
                {
                    Thread[] newthread = new Thread[cores];
                    for (int j = 0; j < cores; j++)
                    {
                        newthread[j] = new Thread(LookSimCV);
                        newthread[j].Start(j);

                    }
                    for (int i = 0; i < cores; i++)
                    {
                        newthread[i].Join();
                    }
                }
                else if (anti == 0)
                {
                    Thread[] newthread = new Thread[cores];
                    for (int j = 0; j < cores; j++)
                    {
                        newthread[j] = new Thread(PathSimAnti);
                        newthread[j].Start(j);

                    }
                    for (int i = 0; i < cores; i++)
                    {
                        newthread[i].Join();
                    }
                }
                else
                {
                    Thread[] newthread = new Thread[cores];
                    for (int j = 0; j < cores; j++)
                    {
                        newthread[j] = new Thread(PathSim);
                        newthread[j].Start(j);

                    }
                    for (int i = 0; i < cores; i++)
                    {
                        newthread[i].Join();
                    }
                }

            }
            else if (optype == 3)
            {
                if (cv == 0 && anti == 0)
                {
                    Thread[] newthread = new Thread[cores];
                    for (int j = 0; j < cores; j++)
                    {
                        newthread[j] = new Thread(RangeSimCVAnti);
                        newthread[j].Start(j);

                    }
                    for (int i = 0; i < cores; i++)
                    {
                        newthread[i].Join();
                    }
                }
                else if (cv == 0)
                {
                    Thread[] newthread = new Thread[cores];
                    for (int j = 0; j < cores; j++)
                    {
                        newthread[j] = new Thread(RangeSimCV);
                        newthread[j].Start(j);

                    }
                    for (int i = 0; i < cores; i++)
                    {
                        newthread[i].Join();
                    }
                }
                else if (anti == 0)
                {
                    Thread[] newthread = new Thread[cores];
                    for (int j = 0; j < cores; j++)
                    {
                        newthread[j] = new Thread(PathSimAnti);
                        newthread[j].Start(j);

                    }
                    for (int i = 0; i < cores; i++)
                    {
                        newthread[i].Join();
                    }
                }
                else
                {
                    Thread[] newthread = new Thread[cores];
                    for (int j = 0; j < cores; j++)
                    {
                        newthread[j] = new Thread(PathSim);
                        newthread[j].Start(j);

                    }
                    for (int i = 0; i < cores; i++)
                    {
                        newthread[i].Join();
                    }
                }

            }
           


            double vj = 0;
            double er = 0;
            double step2 = Convert.ToDouble(steps);
            double sumct2 = 0;
            double[] err = new double[(paths + 1) / 2];
            double[] answer = new double[2];

            if (optype == 0)
            {
                if (cv != 0)
                {
                    for (int i = 0; i < paths + 1; i++)
                    {
                        for (int j = 0; j < steps; j++)
                        {
                            payoff1[i] += payoff2[i, j];
                        }
                        payoff1[i] = payoff1[i] / step2;
                    }
                }


                if (type == 0 && cv != 0)
                {
                    for (int i = 0; i < paths+1; i++)
                    {
                        if (payoff1[i] - k > 0)
                        {
                            vj += payoff1[i] - k;
                            payoff1[i] = payoff1[i] - k;
                        }
                        else
                        {
                            vj += 0;
                            payoff1[i] = 0;
                        }
                    }
                }
                else if (type == 1 && cv != 0)
                {
                    for (int i = 0; i < paths+1; i++)
                    {
                        if (k - payoff1[i] > 0)
                        {
                            vj += k - payoff1[i];
                            payoff1[i] = k - payoff1[i];
                        }
                        else
                        {
                            vj += 0;
                            payoff1[i] = 0;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < paths + 1; i++)
                    {
                        vj += payoff1[i];
                        sumct2 = sumct2 + payoff1[i] * payoff1[i];
                    }
                }
                answer[0] = (vj / paths) * Math.Pow(Math.E, -r * T);
            }
            else if (optype == 1)
            {
                if (type == 0 && cv != 0)
                {
                    for (int i = 0; i < paths + 1; i++)
                    {
                        if (payoff1[i] >= k)
                        {
                            vj += rebate;
                            payoff1[i] = rebate;
                        }
                        else
                        {
                            vj += 0;
                            payoff1[i] = 0;
                        }
                    }
                }
                else if (type == 1 && cv != 0)
                {
                    for (int i = 0; i < paths; i++)
                    {
                        if (payoff1[i] <= k)
                        {
                            vj += rebate;
                            payoff1[i] = rebate;
                        }
                        else
                        {
                            vj += 0;
                            payoff1[i] = 0;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < paths + 1; i++)
                    {
                        vj += payoff1[i];
                        sumct2 = sumct2 + payoff1[i] * payoff1[i];
                    }
                }
                answer[0] = (vj / paths) * Math.Pow(Math.E, -r * T);
            }
            else if (optype == 2)
            {
                if (type == 0 && cv != 0)
                {
                    for (int i = 0; i < paths + 1; i++)
                    {
                        payoff1[i] = s;
                        for (int j = 1; j < steps; j++)
                        {
                            if (payoff2[i, j] > payoff1[i])
                            {
                                payoff1[i] = payoff2[i, j];
                            }

                        }

                    }
                }
                else if (type == 1 && cv != 0)
                {
                    for (int i = 0; i < paths + 1; i++)
                    {
                        payoff1[i] = s;
                        for (int j = 1; j < steps; j++)
                        {
                            if (payoff2[i, j] < payoff1[i])
                            {
                                payoff1[i] = payoff2[i, j];
                            }

                        }

                    }
                }


                if (type == 0 && cv != 0)
                {
                    for (int i = 0; i < paths + 1; i++)
                    {
                        if (payoff1[i] - k > 0)
                        {
                            vj += payoff1[i] - k;
                            payoff1[i] = payoff1[i] - k;
                        }
                        else
                        {
                            vj += 0;
                            payoff1[i] = 0;
                        }
                    }
                }
                else if (type == 1 && cv != 0)
                {
                    for (int i = 0; i < paths + 1; i++)
                    {
                        if (k - payoff1[i] > 0)
                        {
                            vj += k - payoff1[i];
                            payoff1[i] = k - payoff1[i];
                        }
                        else
                        {
                            vj += 0;
                            payoff1[i] = 0;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < paths; i++)
                    {
                        vj += payoff1[i];
                        sumct2 = sumct2 + payoff1[i] * payoff1[i];
                    }
                }
                answer[0] = (vj / paths) * Math.Pow(Math.E, -r * T);
            }
            else if (optype == 3)
            {
                double[] payoff3 = new double[paths + 1];
                if (cv != 0)
                {
                    for (int i = 0; i < paths + 1; i++)
                    {
                        payoff1[i] = s;
                        for (int j = 1; j < steps; j++)
                        {
                            if (payoff2[i, j] > payoff1[i])
                            {
                                payoff1[i] = payoff2[i, j];
                            }

                        }

                    }

                    for (int i = 0; i < paths + 1; i++)
                    {
                        payoff3[i] = s;
                        for (int j = 1; j < steps; j++)
                        {
                            if (payoff2[i, j] < payoff3[i])
                            {
                                payoff3[i] = payoff2[i, j];
                            }

                        }

                    }
                    for (int i = 0; i < paths + 1; i++)
                    {

                        vj += payoff1[i] - payoff3[i];
                        payoff1[i] = payoff1[i] - payoff3[i];

                    }
                }

                else
                {
                    for (int i = 0; i < paths + 1; i++)
                    {
                        vj += payoff1[i];
                        sumct2 = sumct2 + payoff1[i] * payoff1[i];
                    }
                }
                answer[0] = (vj / paths) * Math.Pow(Math.E, -r * T);
            }
            if (anti == 1 && cv != 0)
            {
                for (int i = 0; i < paths; i++)
                {
                    er += Math.Pow(((payoff1[i] * Math.Pow(Math.E, -r * T)) - answer[0]), 2);
                }
                er = Math.Sqrt(er / (paths - 1));
                er = er / Math.Sqrt(paths);
                answer[1] = er;
            }
            else if (anti == 0)

            {
                double var = 0;
                for (int i = 0; i < (paths + 1) / 2; i++)
                {
                    err[i] = (Math.Pow(Math.E, -r * T) * (payoff1[i] + payoff1[i + ((paths + 1) / 2)])) / 2;
                    er += err[i];
                }
                er = er / ((paths + 1) / 2);
                for (int i = 0; i < (paths + 1) / 2; i++)
                {
                    var += Math.Pow((err[i] - er), 2);
                }

                var = var / ((paths + 1) / 2);
                var = var / ((paths + 1) / 2);
                var = Math.Sqrt(var);
                answer[1] = var;
            }

            else
            {
                double sd = Math.Sqrt((sumct2 - vj * vj / paths) * Math.Exp(-2 * r * T) / (paths - 1));
                answer[1] = sd / Math.Sqrt(paths);
            }


            return answer;
        }

        public void EuroSim(object l)
        {
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[] payoff = new double[w];
            double steps1 = Convert.ToDouble(steps);
            double t = T / steps1;
            double[,] sum = new double[w + 4, steps];
            double[,] rand = new double[w + 4, steps];
            if (greek == 0)
            {
                rand = PolarReject(w + 1, steps, x);
            }
            else
            {
                rand = PolarReject1(w + 1, steps, x);
            }

            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < steps; j++)
                {
                    sum[i, j] = rand[i, j];
                }
            }

            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < steps - 1; j++)
                {
                    sum[i, j + 1] = sum[i, j + 1] + sum[i, j];
                }
            }


            for (int i = 0; i < w; i++)
            {
                payoff[i] = s * Math.Pow(Math.E, ((r - ((v * v) / 2)) * T) + ((v * Math.Sqrt(t)) * sum[i, steps - 1]));
            }
            payoff.CopyTo(payoff1, x * w);
        }
        public void PathSim(object l)
        {
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[,] payoff = new double[w, steps];
            double steps1 = Convert.ToDouble(steps);
            double t = T / steps1;
            double[,] rand = new double[w + 4, steps];

            if (greek == 0)
            {
                rand = PolarReject(w + 1, steps, x);
            }
            else
            {
                rand = PolarReject1(w + 1, steps, x);
            }

            for (int i = 0; i < w; i++)
            {
                payoff[i, 0] = s;
                payoff2[i + x * w, 0] = s;
            }

            for (int j = 0; j < w; j++)
            {
                for (int i = 0; i < steps - 1; i++)
                {
                    payoff[j, i + 1] = payoff[j, i] * Math.Pow(Math.E, ((r - ((v * v) / 2)) * t) + ((v * Math.Sqrt(t)) * rand[j, i]));
                    payoff2[ j + x * w, i + 1] = payoff[j, i + 1];
                }
            }

        }
        public void PathSimAnti(object l)
        {
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[,] payoff = new double[w, steps];
            double steps1 = Convert.ToDouble(steps);
            double t = T / steps1;
            double[,] rand = new double[w + 4, steps];

            if (greek == 0)
            {
                rand = PolarReject(w + 1, steps, x);
            }
            else
            {
                rand = PolarReject1(w + 1, steps, x);
            }


            for (int i = 0; i < w; i++)
            {

                payoff[i, 0] = s;
                payoff2[i + x * w, 0] = s;
            }

            for (int i = 0; i < (w + 1) / 2; i++)
            {
                for (int j = 0; j < steps - 1; j++)
                {
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, ((r - ((v * v) / 2)) * t) + ((v * Math.Sqrt(t)) * rand[i, j]));
                    payoff[i + (w + 1) / 2, j + 1] = payoff[i + (w + 1) / 2, j] * Math.Pow(Math.E, ((r - ((v * v) / 2)) * t) + ((v * Math.Sqrt(t)) * (-1 * rand[i, j])));

                    payoff2[i + ((w+1)/2) * x, j + 1] = payoff[i, j + 1];
                    payoff2[i + (paths / 2) + ((w + 1) / 2) * x, j + 1] = payoff[i + (w + 1) / 2, j + 1];
                }
            }

        }
        public void LookSimCV(object l)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[,] delta = new double[w + 2, steps + 1];
            double[,] payoff = new double[w + 2, steps + 1];
            double[] paycv = new double[w];
            double[,] rand = new double[w + 4, steps];
            if (greek == 0)
            {
                rand = PolarReject(w + 1, steps, x);
            }
            else
            {
                rand = PolarReject2(w + 1, steps, x);
            }

            for (int i = 0; i < w; i++)
            {
                payoff[i, 0] = s;
            }

            for (int i = 0; i < w; i++)
            {
                double cv = 0;
                double max = 0;
                double min = s;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta[i, j + 1] = Deltacalc(payoff[i, j], k, v, r, T, t, type);
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    cv = cv + delta[i, j + 1] * (payoff[i, j + 1] - payoff[i, j] * Math.Exp(r * dt));
                    if (type == 0 && payoff[i, j + 1] > max)
                    {
                        max = payoff[i, j + 1];
                    }
                    else if (type == 1 && payoff[i, j + 1] < min)
                    {
                        min = payoff[i, j + 1];
                    }

                }
                //Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)
                if (type == 0)
                {
                    paycv[i] = Math.Max(max - k, 0) + -1 * cv;
                }
                else
                {
                    paycv[i] = Math.Max(k - min, 0) + -1 * cv;
                }
                paycv.CopyTo(payoff1, w * x);

            }


        }
        public void AsianSimCV(object l)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[,] delta = new double[w + 2, steps + 1];
            double[,] payoff = new double[w + 2, steps + 1];
            double[] paycv = new double[w];
            double[,] rand = new double[w + 4, steps];
            if (greek == 0)
            {
                rand = PolarReject(w + 1, steps, x);
            }
            else
            {
                rand = PolarReject2(w + 1, steps, x);
            }
            for (int i = 0; i < w; i++)
            {
                payoff[i, 0] = s;
            }

            for (int i = 0; i < w; i++)
            {
                double cv = 0;
                double avg = s;


                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta[i, j + 1] = Deltacalc(payoff[i, j], k, v, r, T, t, type);
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    cv = cv + delta[i, j + 1] * (payoff[i, j + 1] - payoff[i, j] * Math.Exp(r * dt));
                    avg += payoff[i, j + 1];
                }
                //Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)
                avg = avg / steps1;
                if (type == 0)
                {
                    paycv[i] = Math.Max(avg - k, 0) + -1 * cv;
                }
                else
                {
                    paycv[i] = Math.Max(k - avg, 0) + -1 * cv;
                }


            }
            paycv.CopyTo(payoff1, w * x);


        }
        public void RangeSimCV(object l)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[,] delta = new double[w + 2, steps + 1];
            double[,] payoff = new double[w + 2, steps + 1];
            double[] paycv = new double[w];
            double[,] rand = new double[w + 4, steps];
            if (greek == 0)
            {
                rand = PolarReject(w + 1, steps, x);
            }
            else
            {
                rand = PolarReject2(w + 1, steps, x);
            }
            for (int i = 0; i < w; i++)
            {
                payoff[i, 0] = s;
            }

            for (int i = 0; i < w; i++)
            {
                double cv = 0;
                double max = 0;
                double min = s;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta[i, j + 1] = Deltacalc(payoff[i, j], k, v, r, T, t, type);
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    cv = cv + delta[i, j + 1] * (payoff[i, j + 1] - payoff[i, j] * Math.Exp(r * dt));
                    if (payoff[i, j + 1] > max)
                    {
                        max = payoff[i, j + 1];
                    }
                    if (payoff[i, j + 1] < min)
                    {
                        min = payoff[i, j + 1];
                    }

                }
                //Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)

                paycv[i] = (max - min) + -1 * cv;


            }

            paycv.CopyTo(payoff1, w * x);
        }
        public void DigSimCV(object l)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[,] delta = new double[w + 2, steps + 1];
            double[,] payoff = new double[w + 2, steps + 1];
            double[] paycv = new double[w];
            double[,] rand = new double[w + 4, steps];
            if (greek == 0)
            {
                rand = PolarReject(w + 1, steps, x);
            }
            else
            {
                rand = PolarReject2(w + 1, steps, x);
            }
            for (int i = 0; i < w; i++)
            {
                payoff[i, 0] = s;
            }

            for (int i = 0; i < w; i++)
            {
                double cv = 0;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta[i, j + 1] = Deltacalc(payoff[i, j], k, v, r, T, t, type);
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    cv = cv + delta[i, j + 1] * (payoff[i, j + 1] - payoff[i, j] * Math.Exp(r * dt));

                }
                //Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)

                if (type == 0 && payoff[i, steps] >= k)
                {
                    paycv[i] = rebate - cv;
                }
                else if (type == 1 && payoff[i, steps] <= k)
                {
                    paycv[i] = rebate - cv;
                }
                else
                {
                    paycv[i] = 0 - cv;
                }


            }

            paycv.CopyTo(payoff1, w * x);
        }

        public void DigSimCVAnti(object l)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[,] delta1 = new double[(w + 1) / 2, steps + 1];
            double[,] delta2 = new double[(w + 1) / 2, steps + 1];
            double[,] payoff = new double[(w + 1) / 2, steps + 1];
            double[,] payoff2 = new double[(w + 1) / 2, steps + 1];
            double[] paycv = new double[w];
            double[,] rand = new double[w + 4, steps];
            if (greek == 0)
            {
                rand = PolarRejectAnti(w + 1, steps, x);
            }
            else
            {
                rand = PolarRejectAnti1(w + 1, steps, x);
            }
            for (int i = 0; i < (w + 1) / 2; i++)
            {
                payoff[i, 0] = s;
                payoff2[i, 0] = s;
            }

            for (int i = 0; i < (w + 1) / 2; i++)
            {
                double cv1 = 0;
                double cv2 = 0;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta1[i, j + 1] = Deltacalc(payoff[i, j], k, v, r, T, t, type);
                    delta2[i, j + 1] = Deltacalc(payoff2[i, j], k, v, r, T, t, type);
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    payoff2[i, j + 1] = payoff2[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * (-1 * rand[i, j]));
                    cv1 = cv1 + delta1[i, j + 1] * (payoff[i, j + 1] - payoff[i, j] * Math.Exp(r * dt));
                    cv2 = cv2 + delta2[i, j + 1] * (payoff2[i, j + 1] - payoff2[i, j] * Math.Exp(r * dt));
                }
                // Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)

                if (type == 0 && payoff[i, steps] >= k && payoff2[i, steps] >= k)
                {
                    paycv[i] = (rebate - cv1 + rebate - cv2);
                }
                else if (type == 0 && payoff[i, steps] < k && payoff2[i, steps] < k)
                {
                    paycv[i] = 0 - cv1 - cv2;
                }
                else if (type == 0)
                {
                    paycv[i] = rebate - cv1 - cv2;
                }
                else if (type == 1 && payoff[i, steps] <= k && payoff2[i, steps] <= k)
                {
                    paycv[i] = (rebate - cv1 + rebate - cv2);
                }
                else if (type == 0 && payoff[i, steps] > k && payoff2[i, steps] > k)
                {
                    paycv[i] = 0 - cv1 - cv2;
                }
                else
                {
                    paycv[i] = rebate - cv1 - cv2;
                }
                

            }
                paycv.CopyTo(payoff1, x * w);
        }
        public void RangeSimCVAnti(object l)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[,] delta1 = new double[(w + 1) / 2, steps + 1];
            double[,] delta2 = new double[(w + 1) / 2, steps + 1];
            double[,] payoff = new double[(w + 1) / 2, steps + 1];
            double[,] payoff2 = new double[(w + 1) / 2, steps + 1];
            double[] paycv = new double[w];
            double[,] rand = new double[w + 4, steps];
            if (greek == 0)
            {
                rand = PolarRejectAnti(w + 1, steps, x);
            }
            else
            {
                rand = PolarRejectAnti1(w + 1, steps, x);
            }
            for (int i = 0; i < (w + 1) / 2; i++)
            {
                payoff[i, 0] = s;
                payoff2[i, 0] = s;
            }

            for (int i = 0; i < (w + 1) / 2; i++)
            {
                double cv1 = 0;
                double cv2 = 0;
                double max1 = 0;
                double max2 = 0;
                double min1 = s;
                double min2 = s;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta1[i, j + 1] = Deltacalc(payoff[i, j], k, v, r, T, t, type);
                    delta2[i, j + 1] = Deltacalc(payoff2[i, j], k, v, r, T, t, type);
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    payoff2[i, j + 1] = payoff2[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * (-1 * rand[i, j]));
                    cv1 = cv1 + delta1[i, j + 1] * (payoff[i, j + 1] - payoff[i, j] * Math.Exp(r * dt));
                    cv2 = cv2 + delta2[i, j + 1] * (payoff2[i, j + 1] - payoff2[i, j] * Math.Exp(r * dt));

                    if (payoff[i, j + 1] > max1)
                    {
                        max1 = payoff[i, j + 1];
                    }
                    if (payoff[i, j + 1] < min1)
                    {
                        min1 = payoff[i, j + 1];
                    }
                    if (payoff2[i, j + 1] > max2)
                    {
                        max2 = payoff2[i, j + 1];
                    }
                    if (payoff2[i, j + 1] < min2)
                    {
                        min2 = payoff2[i, j + 1];
                    }
                }
                // Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)

                paycv[i] = (max1 - min1 + -1 * cv1 + max2 - min2 + -1 * cv2);



            }

            paycv.CopyTo(payoff1, x * w);
        }

        public void LookSimCVAnti(object l)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[,] delta1 = new double[(w + 1) / 2, steps + 1];
            double[,] delta2 = new double[(w + 1) / 2, steps + 1];
            double[,] payoff = new double[(w + 1) / 2, steps + 1];
            double[,] payoff2 = new double[(w + 1) / 2, steps + 1];
            double[] paycv = new double[w];
            double[,] rand = new double[w + 4, steps];
            if (greek == 0)
            {
                rand = PolarRejectAnti(w + 1, steps, x);
            }
            else
            {
                rand = PolarRejectAnti1(w + 1, steps, x);
            }
            for (int i = 0; i < (w + 1) / 2; i++)
            {
                payoff[i, 0] = s;
                payoff2[i, 0] = s;
            }

            for (int i = 0; i < (w + 1) / 2; i++)
            {
                double cv1 = 0;
                double cv2 = 0;
                double max1 = 0;
                double max2 = 0;
                double min1 = s;
                double min2 = s;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta1[i, j + 1] = Deltacalc(payoff[i, j], k, v, r, T, t, type);
                    delta2[i, j + 1] = Deltacalc(payoff2[i, j], k, v, r, T, t, type);
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    payoff2[i, j + 1] = payoff2[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * (-1 * rand[i, j]));
                    cv1 = cv1 + delta1[i, j + 1] * (payoff[i, j + 1] - payoff[i, j] * Math.Exp(r * dt));
                    cv2 = cv2 + delta2[i, j + 1] * (payoff2[i, j + 1] - payoff2[i, j] * Math.Exp(r * dt));

                    if (type == 0 && payoff[i, j + 1] > max1)
                    {
                        max1 = payoff[i, j + 1];
                    }
                    else if (type == 1 && payoff[i, j + 1] < min1)
                    {
                        min1 = payoff[i, j + 1];
                    }
                    if (type == 0 && payoff2[i, j + 1] > max2)
                    {
                        max2 = payoff2[i, j + 1];
                    }
                    else if (type == 1 && payoff2[i, j + 1] < min2)
                    {
                        min2 = payoff2[i, j + 1];
                    }
                }
                // Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)

                if (type == 0)
                {
                    paycv[i] = (Math.Max(max1 - k, 0) + -1 * cv1 + Math.Max(max2 - k, 0) + -1 * cv2);
                }
                else
                {
                    paycv[i] = (Math.Max(k - min1, 0) + -1 * cv1 + Math.Max(k - min2, 0) + -1 * cv2);
                }

            }

            paycv.CopyTo(payoff1, x * w);
        }
        public void AsianSimCVAnti(object l)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[,] delta1 = new double[(w + 1) / 2, steps + 1];
            double[,] delta2 = new double[(w + 1) / 2, steps + 1];
            double[,] payoff = new double[(w + 1) / 2, steps + 1];
            double[,] payoff2 = new double[(w + 1) / 2, steps + 1];
            double[] paycv = new double[w];
            double[,] rand = new double[w + 4, steps];
            if (greek == 0)
            {
                rand = PolarRejectAnti(w + 1, steps, x);
            }
            else
            {
                rand = PolarRejectAnti1(w + 1, steps, x);
            }
            for (int i = 0; i < (w + 1) / 2; i++)
            {
                payoff[i, 0] = s;
                payoff2[i, 0] = s;
            }

            for (int i = 0; i < (w + 1) / 2; i++)
            {
                double cv1 = 0;
                double cv2 = 0;
                double avg1 = s;
                double avg2 = s;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta1[i, j + 1] = Deltacalc(payoff[i, j], k, v, r, T, t, type);
                    delta2[i, j + 1] = Deltacalc(payoff2[i, j], k, v, r, T, t, type);
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    payoff2[i, j + 1] = payoff2[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * (-1 * rand[i, j]));
                    cv1 = cv1 + delta1[i, j + 1] * (payoff[i, j + 1] - payoff[i, j] * Math.Exp(r * dt));
                    cv2 = cv2 + delta2[i, j + 1] * (payoff2[i, j + 1] - payoff2[i, j] * Math.Exp(r * dt));
                    avg1 += payoff[i, j + 1];
                    avg2 += payoff2[i, j + 1];
                }
                // Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)
                avg1 = avg1 / steps1;
                avg2 = avg2 / steps1;
                if (type == 0)
                {
                    paycv[i] = (Math.Max(avg1 - k, 0) + -1 * cv1 + Math.Max(avg2 - k, 0) + -1 * cv2);
                }
                else
                {
                    paycv[i] = (Math.Max(k - avg1, 0) + -1 * cv1 + Math.Max(k - avg2, 0) + -1 * cv2);
                }

            }

            paycv.CopyTo(payoff1,x*w);
        }

        public void EuroSimAnti(object l)
        {
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[] payoff3 = new double[(w + 1) / 2];
            double[] payoff2 = new double[(w + 1) / 2];
            double steps1 = Convert.ToDouble(steps);
            double t = T / steps1;
            double[,] sum = new double[w + 1, steps];
            double[,] rand = new double[w + 4, steps];
            if (greek == 0)
            {
                rand = PolarRejectAnti(w + 1, steps, x);
            }
            else
            {
                rand = PolarRejectAnti1(w + 1, steps, x);
            }

            for (int i = 0; i < (w + 1) / 2; i++)
            {
                for (int j = 0; j < steps; j++)
                {
                    sum[i, j] = rand[i, j];
                }
            }

            for (int i = 0; i < (w + 1) / 2; i++)
            {
                for (int j = 0; j < steps - 1; j++)
                {
                    sum[i, j + 1] = sum[i, j + 1] + sum[i, j];
                }
            }


            for (int i = 0; i < (w + 1) / 2; i++)
            {
                payoff3[i] = s * Math.Pow(Math.E, ((r - ((v * v) / 2)) * T) + ((v * Math.Sqrt(t)) * sum[i, steps - 1]));
            }
            for (int i = 0; i < (w + 1) / 2; i++)
            {
                payoff2[i] = s * Math.Pow(Math.E, ((r - ((v * v) / 2)) * T) + ((v * Math.Sqrt(t)) * (-1 * sum[i, steps - 1])));
            }

            payoff3.CopyTo(payoff1,x * w);
            Array.Copy(payoff2,0, payoff1, (2 * x + 1) * payoff3.Length, payoff2.Length);
        }
        public void EuroSimCV(object l)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[,] delta = new double[w + 2, steps + 1];
            double[,] payoff = new double[w + 2, steps + 1];
            double[] paycv = new double[w];
            double[,] rand = new double[w + 4, steps];
            if (greek == 0)
            {
                rand = PolarReject(w + 1, steps, x);
            }
            else
            {
                rand = PolarReject2(w + 1, steps, x);
            }
            double[] temp = new double[w + 2];
            for (int i = 0; i < w; i++)
            {
                payoff[i, 0] = s;
            }

            for (int i = 0; i < w; i++)
            {
                double cv1 = 0;

                for (int j = 0; j < steps; j++)
                {
                    t = j * dt;
                    delta[i, j + 1] = Deltacalc(payoff[i, j], k, v, r, T, t, type);
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    cv1 = cv1 + delta[i, j + 1] * (payoff[i, j + 1] - payoff[i, j] * Math.Exp(r * dt));
                }
                //Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)
                if (type == 0)
                {
                    paycv[i] = Math.Max(payoff[i, steps] - k, 0) + -1 * cv1;
                }
                else
                {
                    paycv[i] = Math.Max(k - payoff[i, steps], 0) + -1 * cv1;
                }
            }
            paycv.CopyTo(payoff1, w * x);

        }

        public void EuroSimCVAnti(object l)
        {
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[,] delta1 = new double[(w + 1) / 2, steps + 1];
            double[,] delta2 = new double[(w + 1) / 2, steps + 1];
            double[,] payoff = new double[(w + 1) / 2, steps + 1];
            double[,] payoff2 = new double[(w + 1) / 2, steps + 1];
            double[] paycv = new double[w];
            double[,] rand = new double[w + 4, steps];
            if (greek == 0)
            {
                rand = PolarRejectAnti(w + 1, steps, x);
            }
            else
            {
                rand = PolarRejectAnti1(w + 1, steps, x);
            }

            for (int i = 0; i < (w + 1) / 2; i++)
            {
                payoff[i, 0] = s;
                payoff2[i, 0] = s;
            }

            for (int i = 0; i < (w + 1) / 2; i++)
            {
                double cv1 = 0;
                double cv2 = 0;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta1[i, j + 1] = Deltacalc(payoff[i, j], k, v, r, T, t, type);
                    delta2[i, j + 1] = Deltacalc(payoff2[i, j], k, v, r, T, t, type);
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    payoff2[i, j + 1] = payoff2[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * (-1 * rand[i, j]));
                    cv1 = cv1 + delta1[i, j + 1] * (payoff[i, j + 1] - payoff[i, j] * Math.Exp(r * dt));
                    cv2 = cv2 + delta2[i, j + 1] * (payoff2[i, j + 1] - payoff2[i, j] * Math.Exp(r * dt));
                }
                // Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)
                if (type == 0)
                {
                    paycv[i] = (Math.Max(payoff[i, steps] - k, 0) + -1 * cv1 + Math.Max(payoff2[i, steps] - k, 0) + -1 * cv2);
                }
                else
                {
                    paycv[i] = (Math.Max(k - payoff[i, steps], 0) + -1 * cv1 + Math.Max(k - payoff2[i, steps], 0) + -1 * cv2);
                }

            }

            paycv.CopyTo(payoff1, w * x);
        }
        public double[,] PolarReject(int p, int s, int x)
        {
            //Declare Variables

            double[,] prand = new double[p + 1, s];
            double rand;
            double rand2;
            double w;
            double c;

            Random v = new Random();
            double rd = v.NextDouble() * 1000;
            Random rnd = new Random(x * 200 * Convert.ToInt32(rd));



            // Create a for loop that creates a matrix of random numbers
            for (int a = 0; a < s; a++)
            {
                for (int b = 0; b <= p - 1; b += 2)
                {

                    // Create a do while function that will refind rand and rand2 if w>1
                    do
                    {
                        rand = 2 * rnd.NextDouble() - 1;
                        rand2 = 2 * rnd.NextDouble() - 1;
                        w = rand * rand + rand2 * rand2;
                    }
                    while (w > 1);
                    //Run the arithmatic to get normally distributed RV's
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    prand[b, a] = c * rand;
                    prand[b + 1, a] = c * rand2;

                }
            }
            //Return the matrix of doubles 
            return prand;
        }
        public double[,] PolarRejectAnti(int p, int s, int x)
        {
            //Declare Variables
            double[,] prand = new double[(p + 3) / 2, s];
            double rand;
            double rand2;
            double w;
            double c;

            Random v = new Random();
            double rd = v.NextDouble() * 1000;
            Random rnd = new Random(x * 200 * Convert.ToInt32(rd));

            // Create a for loop that creates a matrix of random numbers
            for (int a = 0; a < s; a++)
            {
                for (int b = 0; b <= (p - 1) / 2; b += 2)
                {

                    // Create a do while function that will refind rand and rand2 if w>1
                    do
                    {
                        rand = 2 * rnd.NextDouble() - 1;
                        rand2 = 2 * rnd.NextDouble() - 1;
                        w = rand * rand + rand2 * rand2;
                    }
                    while (w > 1);
                    //Run the arithmatic to get normally distributed RV's
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    prand[b, a] = c * rand;
                    prand[b + 1, a] = c * rand2;

                }
            }
            //Return the matrix of doubles 
            return prand;
        }
        public double[,] PolarReject1(int p, int s, int x)
        {
            //Declare Variables

            double[,] prand = new double[p + 1, s];
            double rand;
            double rand2;
            double w;
            double c;


            Random rnd = new Random(x * 43);


            // Create a for loop that creates a matrix of random numbers
            for (int a = 0; a < s; a++)
            {
                for (int b = 0; b <= p - 1; b += 2)
                {

                    // Create a do while function that will refind rand and rand2 if w>1
                    do
                    {
                        rand = 2 * rnd.NextDouble() - 1;
                        rand2 = 2 * rnd.NextDouble() - 1;
                        w = rand * rand + rand2 * rand2;
                    }
                    while (w > 1);
                    //Run the arithmatic to get normally distributed RV's
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    prand[b, a] = c * rand;
                    prand[b + 1, a] = c * rand2;

                }
            }
            //Return the matrix of doubles 
            return prand;
        }
        public double[,] PolarReject2(int p, int s, int x)
        {
            //Declare Variables

            double[,] prand = new double[p + 1, s];
            double rand;
            double rand2;
            double w;
            double c;


            Random rnd = new Random(43);


            // Create a for loop that creates a matrix of random numbers
            for (int a = 0; a < s; a++)
            {
                for (int b = 0; b <= p - 1; b += 2)
                {

                    // Create a do while function that will refind rand and rand2 if w>1
                    do
                    {
                        rand = 2 * rnd.NextDouble() - 1;
                        rand2 = 2 * rnd.NextDouble() - 1;
                        w = rand * rand + rand2 * rand2;
                    }
                    while (w > 1);
                    //Run the arithmatic to get normally distributed RV's
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    prand[b, a] = c * rand;
                    prand[b + 1, a] = c * rand2;

                }
            }
            //Return the matrix of doubles 
            return prand;
        }
        public double[,] PolarRejectAnti1(int p, int s, int x)
        {
            //Declare Variables
            double[,] prand = new double[(p + 3) / 2, s];
            double rand;
            double rand2;
            double w;
            double c;
            Random rnd = new Random(x * 43);

            // Create a for loop that creates a matrix of random numbers
            for (int a = 0; a < s; a++)
            {
                for (int b = 0; b <= (p - 1) / 2; b += 2)
                {

                    // Create a do while function that will refind rand and rand2 if w>1
                    do
                    {
                        rand = 2 * rnd.NextDouble() - 1;
                        rand2 = 2 * rnd.NextDouble() - 1;
                        w = rand * rand + rand2 * rand2;
                    }
                    while (w > 1);
                    //Run the arithmatic to get normally distributed RV's
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    prand[b, a] = c * rand;
                    prand[b + 1, a] = c * rand2;

                }
            }
            //Return the matrix of doubles 
            return prand;
        }
        public double Deltacalc(double s, double k, double v, double r, double T, double t, int type)
        {
            double d1;
            double delta;
            d1 = (Math.Log(s / k) + (r + (Math.Pow(v, 2) / 2)) * (T - t)) / (v * Math.Sqrt(T - t));
            if (type == 0)
            {
                delta = Phi(d1);
            }
            else
            {
                delta = Phi(d1) - 1;
            }
            return delta;
        }


        //This is a cdf normal distribution function that I got from https://www.johndcook.com/blog/csharp_phi/ . I tested it against the cdf norm function in
        // Matlab and it passed the tests. 
        static double Phi(double x)
        {
            // constants
            double a1 = 0.254829592;
            double a2 = -0.284496736;
            double a3 = 1.421413741;
            double a4 = -1.453152027;
            double a5 = 1.061405429;
            double p = 0.3275911;

            // Save the sign of x
            int sign = 1;
            if (x < 0)
                sign = -1;
            x = Math.Abs(x) / Math.Sqrt(2.0);

            // A&S formula 7.1.26
            double t = 1.0 / (1.0 + p * x);
            double y = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.Exp(-x * x);

            return 0.5 * (1.0 + sign * y);
        }

        //Works Cited
        // Cook, John. "Stand-alone C# code for Φ(x)." https://www.johndcook.com/blog/csharp_phi/. Accessed 5 March 2018.

    }
    public class MultiCore2
    {

        public double s = 50;
        public double k = 50;
        public double v = .5;
        public double r = 0.05;
        public double T = 1;
        public int paths = 20;
        public int steps = 2;
        public int cores = 4;
        public int w = 0;
        public int bartype;
        public double bar = 0;
        public int numco;
        public int type;
        public int greek;
        public double[] payoff1 = new double[10000000];


        public double[] Pricing(int type, int cv, int anti)
        {

            if (cv == 0 && anti == 0)
            {
                Thread[] newthread = new Thread[cores];
                for (int j = 0; j < cores; j++)
                {
                    newthread[j] = new Thread(BarSimCVAnti);
                    newthread[j].Start(j);

                }
                for (int i = 0; i < cores; i++)
                {
                    newthread[i].Join();
                }
            }
            else if (cv == 0)
            {
                Thread[] newthread = new Thread[cores];
                for (int j = 0; j < cores; j++)
                {
                    newthread[j] = new Thread(BarSimCV);
                    newthread[j].Start(j);

                }
                for (int i = 0; i < cores; i++)
                {
                    newthread[i].Join();
                }
            }
            else if (anti == 0)
            {
                Thread[] newthread = new Thread[cores];
                for (int j = 0; j < cores; j++)
                {
                    newthread[j] = new Thread(BarSimAnti);
                    newthread[j].Start(j);

                }
                for (int i = 0; i < cores; i++)
                {
                    newthread[i].Join();
                }
            }
            else
            {
                Thread[] newthread = new Thread[cores];
                for (int j = 0; j < cores; j++)
                {
                    newthread[j] = new Thread(BarSim);
                    newthread[j].Start(j);

                }
                for (int i = 0; i < cores; i++)
                {
                    newthread[i].Join();
                }
            }

                double vj = 0;
                double er = 0;
                double sumct2 = 0;
                double[] err = new double[(paths + 1) / 2];
                double[] answer = new double[2];
                double[,] payoff = new double[paths + 1, steps];
                double steps2 = Convert.ToDouble(steps);
                Simulator sim = new Simulator();


                for (int i = 0; i < paths + 1; i++)
                {
                    vj += payoff1[i];
                    sumct2 = sumct2 + payoff1[i] * payoff1[i];
                }

                answer[0] = (vj / paths) * Math.Pow(Math.E, -r * T);
                if (anti == 1 && cv != 0)
                {
                    for (int i = 0; i < paths; i++)
                    {
                        er += Math.Pow(((payoff1[i] * Math.Pow(Math.E, -r * T)) - answer[0]), 2);
                    }
                    er = Math.Sqrt(er / (paths - 1));
                    er = er / Math.Sqrt(paths);
                    answer[1] = er;
                }
                else if (anti == 0)

                {
                    double var = 0;
                    for (int i = 0; i < (paths + 1) / 2; i++)
                    {
                        err[i] = (Math.Pow(Math.E, -r * T) * (payoff1[i] + payoff1[i + ((paths + 1) / 2)])) / 2;
                        er += err[i];
                    }
                    er = er / ((paths + 1) / 2);
                    for (int i = 0; i < (paths + 1) / 2; i++)
                    {
                        var += Math.Pow((err[i] - er), 2);
                    }

                    var = var / ((paths + 1) / 2);
                    var = var / ((paths + 1) / 2);
                    var = Math.Sqrt(var);
                    answer[1] = var;
                }

                else
                {
                    double sd = Math.Sqrt((sumct2 - vj * vj / paths) * Math.Exp(-2 * r * T) / (paths - 1));
                    answer[1] = sd / Math.Sqrt(paths);
                }


                return answer;
            }
        

        public void BarSim(object l)
        {
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[,] payoff = new double[w + 1, steps];
            double[] payoff2 = new double[w+ 1];
            double steps1 = Convert.ToDouble(steps);
            double t = T / steps1;

            double[,] sum = new double[w + 4, steps];
            double[,] rand = new double[w + 4, steps];

            if (greek == 0)
            {
                rand = PolarReject(w + 1, steps, x);
            }
            else
            {
                rand = PolarReject1(w + 1, steps, x);
            }
            for (int i = 0; i < w + 1; i++)
            {
                payoff[i, 0] = s;
            }

            for (int j = 0; j < w; j++)
            {
                double max = s;
                double min = s;
                for (int i = 0; i < steps - 1; i++)
                {
                    payoff[j, i + 1] = payoff[j, i] * Math.Pow(Math.E, ((r - ((v * v) / 2)) * t) + ((v * Math.Sqrt(t)) * rand[j, i]));
                    if (max < payoff[j, i + 1])
                    {
                        max = payoff[j, i + 1];
                    }
                    if (min > payoff[j, i + 1])
                    {
                        min = payoff[j, i + 1];
                    }
                }
                if (bartype == 0 && max >= bar)
                {
                    if (type == 0)
                    {
                        payoff2[j] = Math.Max(payoff[j, steps - 1] - k, 0);
                    }
                    else if (type == 1)
                    {
                        payoff2[j] = Math.Max(k - payoff[j, steps - 1], 0);

                    }

                }
                else if (bartype == 0)
                {
                    payoff2[j] = 0;
                }
                else if (bartype == 1 && max >= bar)
                {
                    payoff2[j] = 0;
                }
                else if (bartype == 1)
                {
                    if (type == 0)
                    {
                        payoff2[j] = Math.Max(payoff[j, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff2[j] = Math.Max(k - payoff[j, steps - 1], 0);
                    }
                }
                else if (bartype == 2 && min <= bar)
                {
                    if (type == 0)
                    {
                        payoff2[j] = Math.Max(payoff[j, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff2[j] = Math.Max(k - payoff[j, steps - 1], 0);
                    }
                }
                else if (bartype == 2)
                {
                    payoff2[j] = 0;
                }
                else if (bartype == 3 && min <= bar)
                {
                    payoff2[j] = 0;
                }
                else
                {
                    if (type == 0)
                    {
                        payoff2[j] = Math.Max(payoff[j, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff2[j] = Math.Max(k - payoff[j, steps - 1], 0);
                    }
                }
            }
            payoff2.CopyTo(payoff1, x * w);
        }
        public void BarSimAnti(object l)
        {
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double[,] payoff2 = new double[w + 2, steps];
            double[] payoff = new double[w + 2];
            double steps1 = Convert.ToDouble(steps);
            double t = T / steps1;

            double[] payoff3 = new double[(w + 1) / 2];

            double[,] sum = new double[w + 1, steps];
            double[,] rand = new double[w + 4, steps];
            if (greek == 0)
            {
                rand = PolarRejectAnti(w + 1, steps, x);
            }
            else
            {
                rand = PolarRejectAnti1(w + 1, steps, x);
            }

            for (int i = 0; i < w + 2; i++)
            {

                payoff2[i, 0] = s;
            }

            for (int i = 0; i < (w + 1) / 2; i++)
            {
                double max1 = s;
                double max2 = s;
                double min1 = s;
                double min2 = s;
                for (int j = 0; j < steps - 1; j++)
                {
                    payoff2[i, j + 1] = payoff2[i, j] * Math.Pow(Math.E, ((r - ((v * v) / 2)) * t) + ((v * Math.Sqrt(t)) * rand[i, j]));
                    payoff2[i + (w + 1) / 2, j + 1] = payoff2[i + (w + 1) / 2, j] * Math.Pow(Math.E, ((r - ((v * v) / 2)) * t) + ((v * Math.Sqrt(t)) * (-1 * rand[i, j])));
                    if (max1 < payoff2[i, j + 1])
                    {
                        max1 = payoff2[i, j + 1];
                    }
                    if (min1 > payoff2[i, j + 1])
                    {
                        min1 = payoff2[i, j + 1];
                    }
                    if (max2 < payoff2[i + (w + 1) / 2, j + 1])
                    {
                        max2 = payoff2[i + (w + 1) / 2, j + 1];
                    }
                    if (min2 > payoff2[i + (w + 1) / 2, j + 1])
                    {
                        min2 = payoff2[i + (w + 1) / 2, j + 1];
                    }
                }
                if (bartype == 0 && max1 >= bar)
                {
                    if (type == 0)
                    {
                        payoff[i] = Math.Max(payoff2[i, steps - 1] - k, 0);
                    }
                    else if (type == 1)
                    {
                        payoff[i] = Math.Max(k - payoff2[i, steps - 1], 0);

                    }

                }
                else if (bartype == 0)
                {
                    payoff[i] = 0;
                }
                else if (bartype == 1 && max1 >= bar)
                {
                    payoff[i] = 0;
                }
                else if (bartype == 1)
                {
                    if (type == 0)
                    {
                        payoff[i] = Math.Max(payoff2[i, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff[i] = Math.Max(k - payoff2[i, steps - 1], 0);
                    }
                }
                else if (bartype == 2 && min1 <= bar)
                {
                    if (type == 0)
                    {
                        payoff[i] = Math.Max(payoff2[i, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff[i] = Math.Max(k - payoff2[i, steps - 1], 0);
                    }
                }
                else if (bartype == 2)
                {
                    payoff[i] = 0;
                }
                else if (bartype == 3 && min1 <= bar)
                {
                    payoff[i] = 0;
                }
                else
                {
                    if (type == 0)
                    {
                        payoff[i] = Math.Max(payoff2[i, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff[i] = Math.Max(k - payoff2[i, steps - 1], 0);
                    }
                }

                if (bartype == 0 && max2 >= bar)
                {
                    if (type == 0)
                    {
                        payoff[i + (w + 1) / 2] = Math.Max(payoff2[i + (w + 1) / 2, steps - 1] - k, 0);
                    }
                    else if (type == 1)
                    {
                        payoff[i + (w + 1) / 2] = Math.Max(k - payoff2[i + (w + 1) / 2, steps - 1], 0);

                    }

                }
                else if (bartype == 0)
                {
                    payoff[i + (w + 1) / 2] = 0;
                }
                else if (bartype == 1 && max2 >= bar)
                {
                    payoff[i + (w + 1) / 2] = 0;
                }
                else if (bartype == 1)
                {
                    if (type == 0)
                    {
                        payoff[i + (w + 1) / 2] = Math.Max(payoff2[i + (w + 1) / 2, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff[i + (w + 1) / 2] = Math.Max(k - payoff2[i + (w + 1) / 2, steps - 1], 0);
                    }
                }
                else if (bartype == 2 && min2 <= bar)
                {
                    if (type == 0)
                    {
                        payoff[i + (w + 1) / 2] = Math.Max(payoff2[i + (w + 1) / 2, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff[i + (w + 1) / 2] = Math.Max(k - payoff2[i + (w + 1) / 2, steps - 1], 0);
                    }
                }
                else if (bartype == 2)
                {
                    payoff[i + (w + 1) / 2] = 0;
                }
                else if (bartype == 3 && min2 <= bar)
                {
                    payoff[i + (w + 1) / 2] = 0;
                }
                else
                {
                    if (type == 0)
                    {
                        payoff[i + (w + 1) / 2] = Math.Max(payoff2[i + (w + 1) / 2, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff[i + (w + 1) / 2] = Math.Max(k - payoff2[i + (w + 1) / 2, steps - 1], 0);
                    }
                }
            }



            payoff.CopyTo(payoff1, x * w);


        }
        public void BarSimCV(object l)
        {
            numco = Convert.ToInt32(l);
            int x = Convert.ToInt32(l);
            w = paths / cores;
            double steps1 = Convert.ToDouble(steps);
            double dt = T / steps1;
            double t;
            double[] payoff2 = new double[w];
            double[,] delta = new double[w + 2, steps + 1];
            double[,] payoff = new double[w + 2, steps + 1];
            double[] paycv = new double[w];
            double[,] rand = new double[w + 4, steps];
            if (greek == 0)
            {
                rand = PolarReject(w + 1, steps, x);
            }
            else
            {
                rand = PolarReject2(w + 1, steps, x);
            }
            for (int i = 0; i < w; i++)
            {
                payoff[i, 0] = s;
            }

            for (int i = 0; i < w; i++)
            {
                double cv = 0;
                double max = s;
                double min = s;

                for (int j = 0; j < steps; j++)
                {
                    t = (j) * dt;
                    delta[i, j + 1] = Deltacalc(payoff[i, j], k, v, r, T, t, type);
                    payoff[i, j + 1] = payoff[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
                    cv = cv + delta[i, j + 1] * (payoff[i, j + 1] - payoff[i, j] * Math.Exp(r * dt));
                    if (payoff[i, j + 1] > max)
                    {
                        max = payoff[i, j + 1];
                    }
                    else if (payoff[i, j + 1] < min)
                    {
                        min = payoff[i, j + 1];
                    }

                }
                //Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)
                if (bartype == 0 && max >= bar)
                {
                    if (type == 0)
                    {
                        payoff2[i] = Math.Max(payoff[i, steps - 1] - k, 0) - cv;
                    }
                    else if (type == 1)
                    {
                        payoff2[i] = Math.Max(k - payoff[i, steps - 1], 0) - cv;

                    }

                }
                else if (bartype == 0)
                {
                    payoff2[i] = 0;
                }
                else if (bartype == 1 && max >= bar)
                {
                    payoff2[i] = 0;
                }
                else if (bartype == 1)
                {
                    if (type == 0)
                    {
                        payoff2[i] = Math.Max(payoff[i, steps - 1] - k, 0) - cv;
                    }
                    else
                    {
                        payoff2[i] = Math.Max(k - payoff[i, steps - 1], 0) - cv;
                    }
                }
                else if (bartype == 2 && min <= bar)
                {
                    if (type == 0)
                    {
                        payoff2[i] = Math.Max(payoff[i, steps - 1] - k, 0) - cv;
                    }
                    else
                    {
                        payoff2[i] = Math.Max(k - payoff[i, steps - 1], 0) - cv;
                    }
                }
                else if (bartype == 2)
                {
                    payoff2[i] = 0;
                }
                else if (bartype == 3 && min <= bar)
                {
                    payoff2[i] = 0;
                }
                else
                {
                    if (type == 0)
                    {
                        payoff2[i] = Math.Max(payoff[i, steps - 1] - k, 0) - cv;
                    }
                    else
                    {
                        payoff2[i] = Math.Max(k - payoff[i, steps - 1], 0) - cv;
                    }
                }
            }

            payoff2.CopyTo(payoff1, x * w);
        }
        public void BarSimCVAnti(object l)
{
    numco = Convert.ToInt32(l);
    int x = Convert.ToInt32(l);
    w = paths / cores;
    double steps1 = Convert.ToDouble(steps);
    double dt = T / steps1;
    double t;
    double[,] delta1 = new double[(w + 1) / 2, steps + 1];
    double[,] delta2 = new double[(w + 1) / 2, steps + 1];
    double[,] payoff1 = new double[(w + 1) / 2, steps + 1];
    double[,] payoff2 = new double[(w + 1) / 2, steps + 1];
    double[] paycv = new double[w];
    double[,] rand = new double[w + 4, steps];

    if (greek == 0)
            {
                rand = PolarRejectAnti(w + 1, steps, x);
            }
    else
            {
                rand = PolarRejectAnti1(w + 1, steps, x);
            }


   for (int i = 0; i < (w + 1) / 2; i++)
    {
        payoff1[i, 0] = s;
        payoff2[i, 0] = s;
    }

    for (int i = 0; i < (w + 1) / 2; i++)
    {
        double cv1 = 0;
        double cv2 = 0;
        double max1 = 0;
        double max2 = 0;
        double min1 = s;
        double min2 = s;

        for (int j = 0; j < steps; j++)
        {
            t = (j) * dt;
            delta1[i, j + 1] = Deltacalc(payoff1[i, j], k, v, r, T, t, type);
            delta2[i, j + 1] = Deltacalc(payoff2[i, j], k, v, r, T, t, type);
            payoff1[i, j + 1] = payoff1[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * rand[i, j]);
            payoff2[i, j + 1] = payoff2[i, j] * Math.Pow(Math.E, (r - ((v * v) / 2)) * dt + v * Math.Sqrt(dt) * (-1 * rand[i, j]));
            cv1 = cv1 + delta1[i, j + 1] * (payoff1[i, j + 1] - payoff1[i, j] * Math.Exp(r * dt));
            cv2 = cv2 + delta2[i, j + 1] * (payoff2[i, j + 1] - payoff2[i, j] * Math.Exp(r * dt));

            if (payoff1[i, j + 1] > max1)
            {
                max1 = payoff1[i, j + 1];
            }
            else if (payoff1[i, j + 1] < min1)
            {
                min1 = payoff1[i, j + 1];
            }
            if (payoff2[i, j + 1] > max2)
            {
                max2 = payoff2[i, j + 1];
            }
            else if (payoff2[i, j + 1] < min2)
            {
                min2 = payoff2[i, j + 1];
            }
        }
        //Can use beta = -1 since we know delta (Implementing Derivatives Models by Clewlow and Strickland)
        if (bartype == 0 && max1 >= bar && max2 >= bar)
        {
            if (type == 0)
            {
                paycv[i] = Math.Max(payoff1[i, steps - 1] - k, 0) - cv1 + Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
            }
            else if (type == 1)
            {
                paycv[i] = Math.Max(k - payoff1[i, steps - 1], 0) - cv1 + Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

            }

        }
        else if (bartype == 0 && max1 >= bar && max2 < bar)
        {
            if (type == 0)
            {
                paycv[i] = Math.Max(payoff1[i, steps - 1] - k, 0) - cv1;
            }
            else if (type == 1)
            {
                paycv[i] = Math.Max(k - payoff1[i, steps - 1], 0) - cv1;

            }
        }
        else if (bartype == 0 && max1 < bar && max2 >= bar)
        {
            if (type == 0)
            {
                paycv[i] = Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
            }
            else if (type == 1)
            {
                paycv[i] = Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

            }
        }
        else if (bartype == 0)
        {
            paycv[i] = 0;
        }
        else if (bartype == 1 && max1 >= bar && max2 >= bar)
        {
            paycv[i] = 0;
        }
        else if (bartype == 1 && max1 >= bar && max2 < bar)
        {
            if (type == 0)
            {
                paycv[i] = Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
            }
            else if (type == 1)
            {
                paycv[i] = Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

            }
        }
        else if (bartype == 1 && max1 < bar && max2 >= bar)
        {
            if (type == 0)
            {
                paycv[i] = Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
            }
            else if (type == 1)
            {
                paycv[i] = Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

            }
        }
        else if (bartype == 1)
        {
            if (type == 0)
            {
                paycv[i] = Math.Max(payoff1[i, steps - 1] - k, 0) - cv1 + Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
            }
            else if (type == 1)
            {
                paycv[i] = Math.Max(k - payoff1[i, steps - 1], 0) - cv1 + Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

            }

        }
        if (bartype == 2 && min1 >= bar && min2 >= bar)
        {
            paycv[1] = 0;

        }
        else if (bartype == 2 && min1 >= bar && min2 < bar)
        {
            if (type == 0)
            {
                paycv[i] = Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
            }
            else if (type == 1)
            {
                paycv[i] = Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

            }
        }
        else if (bartype == 2 && min1 < bar && min2 >= bar)
        {
            if (type == 0)
            {
                paycv[i] = Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
            }
            else if (type == 1)
            {
                paycv[i] = Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

            }
        }
        else if (bartype == 2)
        {
            if (type == 0)
            {
                paycv[i] = Math.Max(payoff1[i, steps - 1] - k, 0) - cv1 + Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
            }
            else if (type == 1)
            {
                paycv[i] = Math.Max(k - payoff1[i, steps - 1], 0) - cv1 + Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

            }

        }
        if (bartype == 3 && min1 >= bar && min2 >= bar)
        {
            if (type == 0)
            {
                paycv[i] = Math.Max(payoff1[i, steps - 1] - k, 0) - cv1 + Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
            }
            else if (type == 1)
            {
                paycv[i] = Math.Max(k - payoff1[i, steps - 1], 0) - cv1 + Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

            }

        }
        else if (bartype == 3 && min1 >= bar && min2 < bar)
        {
            if (type == 0)
            {
                paycv[i] = Math.Max(payoff1[i, steps - 1] - k, 0) - cv1;
            }
            else if (type == 1)
            {
                paycv[i] = Math.Max(k - payoff1[i, steps - 1], 0) - cv1;

            }
        }
        else if (bartype == 3 && min1 < bar && min2 >= bar)
        {
            if (type == 0)
            {
                paycv[i] = Math.Max(payoff2[i, steps - 1] - k, 0) - cv2;
            }
            else if (type == 1)
            {
                paycv[i] = Math.Max(k - payoff2[i, steps - 1], 0) - cv2;

            }
        }
        else if (bartype == 3)
        {
            paycv[i] = 0;
        }

    }

            paycv.CopyTo(payoff1, x * w);
}

        public double[,] PolarReject(int p, int s, int x)
        {
            //Declare Variables

            double[,] prand = new double[p + 1, s];
            double rand;
            double rand2;
            double w;
            double c;

            Random v = new Random();
            double rd = v.NextDouble() * 1000;
            Random rnd = new Random(x * 200 * Convert.ToInt32(rd));



            // Create a for loop that creates a matrix of random numbers
            for (int a = 0; a < s; a++)
            {
                for (int b = 0; b <= p - 1; b += 2)
                {

                    // Create a do while function that will refind rand and rand2 if w>1
                    do
                    {
                        rand = 2 * rnd.NextDouble() - 1;
                        rand2 = 2 * rnd.NextDouble() - 1;
                        w = rand * rand + rand2 * rand2;
                    }
                    while (w > 1);
                    //Run the arithmatic to get normally distributed RV's
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    prand[b, a] = c * rand;
                    prand[b + 1, a] = c * rand2;

                }
            }
            //Return the matrix of doubles 
            return prand;
        }
        public double[,] PolarRejectAnti(int p, int s, int x)
        {
            //Declare Variables
            double[,] prand = new double[(p + 3) / 2, s];
            double rand;
            double rand2;
            double w;
            double c;

            Random v = new Random();
            double rd = v.NextDouble() * 1000;
            Random rnd = new Random(x * 200 * Convert.ToInt32(rd));

            // Create a for loop that creates a matrix of random numbers
            for (int a = 0; a < s; a++)
            {
                for (int b = 0; b <= (p - 1) / 2; b += 2)
                {

                    // Create a do while function that will refind rand and rand2 if w>1
                    do
                    {
                        rand = 2 * rnd.NextDouble() - 1;
                        rand2 = 2 * rnd.NextDouble() - 1;
                        w = rand * rand + rand2 * rand2;
                    }
                    while (w > 1);
                    //Run the arithmatic to get normally distributed RV's
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    prand[b, a] = c * rand;
                    prand[b + 1, a] = c * rand2;

                }
            }
            //Return the matrix of doubles 
            return prand;
        }
        public double[,] PolarReject1(int p, int s, int x)
        {
            //Declare Variables

            double[,] prand = new double[p + 1, s];
            double rand;
            double rand2;
            double w;
            double c;


            Random rnd = new Random(x * 43);


            // Create a for loop that creates a matrix of random numbers
            for (int a = 0; a < s; a++)
            {
                for (int b = 0; b <= p - 1; b += 2)
                {

                    // Create a do while function that will refind rand and rand2 if w>1
                    do
                    {
                        rand = 2 * rnd.NextDouble() - 1;
                        rand2 = 2 * rnd.NextDouble() - 1;
                        w = rand * rand + rand2 * rand2;
                    }
                    while (w > 1);
                    //Run the arithmatic to get normally distributed RV's
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    prand[b, a] = c * rand;
                    prand[b + 1, a] = c * rand2;

                }
            }
            //Return the matrix of doubles 
            return prand;
        }
        public double[,] PolarReject2(int p, int s, int x)
        {
            //Declare Variables

            double[,] prand = new double[p + 1, s];
            double rand;
            double rand2;
            double w;
            double c;


            Random rnd = new Random(43);


            // Create a for loop that creates a matrix of random numbers
            for (int a = 0; a < s; a++)
            {
                for (int b = 0; b <= p - 1; b += 2)
                {

                    // Create a do while function that will refind rand and rand2 if w>1
                    do
                    {
                        rand = 2 * rnd.NextDouble() - 1;
                        rand2 = 2 * rnd.NextDouble() - 1;
                        w = rand * rand + rand2 * rand2;
                    }
                    while (w > 1);
                    //Run the arithmatic to get normally distributed RV's
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    prand[b, a] = c * rand;
                    prand[b + 1, a] = c * rand2;

                }
            }
            //Return the matrix of doubles 
            return prand;
        }
        public double[,] PolarRejectAnti1(int p, int s, int x)
        {
            //Declare Variables
            double[,] prand = new double[(p + 3) / 2, s];
            double rand;
            double rand2;
            double w;
            double c;
            Random rnd = new Random(x * 43);

            // Create a for loop that creates a matrix of random numbers
            for (int a = 0; a < s; a++)
            {
                for (int b = 0; b <= (p - 1) / 2; b += 2)
                {

                    // Create a do while function that will refind rand and rand2 if w>1
                    do
                    {
                        rand = 2 * rnd.NextDouble() - 1;
                        rand2 = 2 * rnd.NextDouble() - 1;
                        w = rand * rand + rand2 * rand2;
                    }
                    while (w > 1);
                    //Run the arithmatic to get normally distributed RV's
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    prand[b, a] = c * rand;
                    prand[b + 1, a] = c * rand2;

                }
            }
            //Return the matrix of doubles 
            return prand;
        }
        public double Deltacalc(double s, double k, double v, double r, double T, double t, int type)
        {
            double d1;
            double delta;
            d1 = (Math.Log(s / k) + (r + (Math.Pow(v, 2) / 2)) * (T - t)) / (v * Math.Sqrt(T - t));
            if (type == 0)
            {
                delta = Phi(d1);
            }
            else
            {
                delta = Phi(d1) - 1;
            }
            return delta;
        }


        //This is a cdf normal distribution function that I got from https://www.johndcook.com/blog/csharp_phi/ . I tested it against the cdf norm function in
        // Matlab and it passed the tests. 
        static double Phi(double x)
        {
            // constants
            double a1 = 0.254829592;
            double a2 = -0.284496736;
            double a3 = 1.421413741;
            double a4 = -1.453152027;
            double a5 = 1.061405429;
            double p = 0.3275911;

            // Save the sign of x
            int sign = 1;
            if (x < 0)
                sign = -1;
            x = Math.Abs(x) / Math.Sqrt(2.0);

            // A&S formula 7.1.26
            double t = 1.0 / (1.0 + p * x);
            double y = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.Exp(-x * x);

            return 0.5 * (1.0 + sign * y);
        }

        //Works Cited
        // Cook, John. "Stand-alone C# code for Φ(x)." https://www.johndcook.com/blog/csharp_phi/. Accessed 5 March 2018.

    }
}

